<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$MESS['REPORT_DETAIL_DEADLINE'] = 'Дедлайн на выполнение фотоотчета';
$MESS['REPORT_DETAIL_REQUIREMENTS'] = 'Требования к Фотоотчету';
$MESS['REPORT_DETAIL_MIN_PHOTO_COUNT'] = 'Минимальное кол-во фото';
$MESS['REPORT_DETAIL_ORIENTATION'] = 'Ориентация фотографий';
$MESS['REPORT_DETAIL_EXAMPLES'] = 'Примеры фотографий';
$MESS['REPORT_DETAIL_OVERVIEW'] = 'Описание';

$MESS['REPORT_DETAIL_BUTTON_APPROVE_REPORT'] = 'Принять фотоотчет';

$MESS['REPORT_DETAIL_BUTTON_REWORK_REPORT'] = 'Вернуть на доработку';
$MESS['REPORT_DETAIL_BUTTON_NEW_DEADLINE'] = 'Дедлайн на доработку';
$MESS['REPORT_DETAIL_BUTTON_SAVE'] = 'Сохранить';

$MESS['REPORT_DETAIL_DESCRIPTION'] = 'Это повторная проверка, поэтому на странице есть
- фотографии, помеченные как согласованные
- уже имеющиеся комментарии от менеджера и возможность добавления повторных
- дедлайн на доработку уже выставлен и не может быть скорректирован';
