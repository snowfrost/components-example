<? use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

?>
<form enctype="multipart/form-data"
      method="POST"
      action="/photoreports/report/<?= $arResult['DATA']['ID'] ?>/photos/">

  <div class="photoreports-detail-user__head">
      <span class="photoreports-detail-user__head-title"> <?= Loc::getMessage('REPORT_DETAIL_DEADLINE') ?>
          : <span class="photoreports-detail-user__head-date"><?= $arResult['DATA']['DEADLINE']; ?></span></span>
      <span class="photoreports-detail__head-field">Тип: <span class="bold">Полный ежемесячный фотоотчет</span></span>
      <span class="photoreports-detail__head-field">Статус: <span class="bold"><?= $arResult['DATA']['STATUS']['VALUE'] ?></span></span>
      <span class="photoreports-detail__head-field">Постановщик задачи: <span class="bold"><?=$arResult['DATA']['USER_CREATE']?></span></span>
  </div>

  <div class="photoreports-detail__requirements-wrapper">
      <div class="photoreports-detail__requirements">
          <div class="title-2"><?= Loc::getMessage('REPORT_DETAIL_REQUIREMENTS') ?></div>
          <ul class="photoreports-detail__requirements-list">
              <li class="photoreports-detail__requirements-list-item">
                  <span class="photoreports-detail__requirements-list-title"><?= Loc::getMessage('REPORT_DETAIL_MIN_PHOTO_COUNT') ?> </span>
                  <span> <?= $arResult['DATA']['TASK']['MIN_PHOTO_COUNT']?: 'Отсутствует'; ?></span>
              </li>
              <li class="photoreports-detail__requirements-list-item">
                  <span class="photoreports-detail__requirements-list-title"><?= Loc::getMessage('REPORT_DETAIL_ORIENTATION') ?> </span>
                  <span><?= $arResult['DATA']['TASK']['ORIENTATION'] ?: 'Отсутствует'; ?></span>
              </li>
              <li class="photoreports-detail__requirements-list-item">
                  <span class="photoreports-detail__requirements-list-title">
                      <?= Loc::getMessage('REPORT_DETAIL_EXAMPLES') ?>
                  </span>
                  <div class="popup-gallery photoreports-detail__requirements-photos">
                      <? if (is_array($arResult['DATA']['TASK']['EXAMPLES']) &&( count($arResult['DATA']['TASK']['EXAMPLES']) > 0)): ?>
                          <? foreach ($arResult['DATA']['TASK']['EXAMPLES'] as $key => $value): ?>
                              <div class="photoreports-detail__requirements-photos-item">
                                  <a href="<?= CFile::GetPath($value) ?>" id="req_img<?= $key ?>">
                                      <img src="<?= CFile::GetPath($value) ?>"
                                           data-src="<?= CFile::GetPath($value) ?>"
                                           class="lazy-load white-popup"
                                           alt="" />
                                  </a>
                                  <span class="popup-trigger">
                                      <svg class='i-icon'>
                                          <use xlink:href='#icon-search' />
                                      </svg>
                                  </span>
                              </div>
                          <? endforeach; ?>
                      <?else:?>
                          Отсутствует
                      <? endif ?>
                  </div>
              </li>
              <li class="photoreports-detail__requirements-list-item">
                  <span class="photoreports-detail__requirements-list-title"> <?= Loc::getMessage('REPORT_DETAIL_OVERVIEW') ?></span>
                  <span><?= $arResult['DATA']['TASK']['PREVIEW_TEXT']?: 'Отсутствует'; ?></span>
              </li>
              <li class="photoreports-detail__requirements-list-item">
                  <span class="photoreports-detail__requirements-list-title"> ID </span>
                  <span><?= $arResult['DATA']['ID'] ?></span>
              </li>
          </ul>
      </div>
      <?if($arResult['DATA']['CONTROL_DATE']):?>
          <div class="photoreports-detail__requirements">
              <div class="photoreports-detail__requirements-deadline-title">
                  <img src="/local/templates/light_red/client/icons/warning.svg" alt="">
                  <span>Дедлайн на доработку: <span class="bold red"><?=$arResult['DATA']['CONTROL_DATE']?></span></span>
              </div>
              <span class="photoreports-detail__requirements-deadline-description bold">Это повторная проверка, поэтому на странице есть:</span>
              <ul class="list-styled">
                  <li>фотографии, помеченные как согласованные</li>
                  <li>уже имеющиеся комментарии от менеджера и возможность добавления повторных</li>
                  <li>дедлайн на доработку уже выставлен и не  может быть скорректирован</li>
              </ul>
          </div>
      <?endif?>
  </div>

  <div class="photoreports-detail__inner">
      <? foreach ($arResult['DATA']['PHOTOS'] as $key => $photo):?>
          <? if ($photo['ACTIVE']): ?>
              <div class="photoreports-detail__item photoreports-detail__item--small">
                  <a href="<?= $photo['DETAIL_URL'] ?>"
                     class="photoreports-detail__item-img <?= ($photo['IS_APPROVE'] || $arResult['STATUS_APPROVED']) ? 'photoreports-detail__item-img--success' : '' ?>">
                      <div class="hover"></div>
                      <div class="photoreports-detail__item-toolbar">
                          <div class="photoreports-detail__item-label"><?= $photo['NUMBER'] ?></div>
                      </div>
                      <img src="<?= $photo['SRC'] ?>"
                           data-src="images/detail.png"
                           class="lazy-load"
                           alt="">
                  </a>

                  <? if (($photo['IS_APPROVE'])||($arResult['STATUS_APPROVED'])): ?>
                      <div class="photoreports-detail__item-title success">
                          <svg class='i-icon'>
                              <use xlink:href='#icon-success' />
                          </svg>
                          ФОТО СОГЛАСОВАНО
                      </div>
                  <? elseif (($arResult['DATA']['CONTROL_DATE']) != ""):?>
                      <div class="photoreports-detail__item-title nosuccess">
                          ФОТО НЕ СОГЛАСОВАНО
                      </div>
                  <? endif ?>
                  <div class="photoreports-detail__comments">
                      <div class="photoreports-detail__comments-title">
                          <a href="<?= $photo['DETAIL_URL'] ?>" style="color: #000;">Комментарии(<?=count($photo['COMMENTS'])?>)</a>
                      </div>
                      <div class="photoreports-detail__comments-items">
                          <?foreach($photo['COMMENTS'] as $commentKey => $comment ):?>
                              <div class="photoreports-detail__comments-item">
                                  <p class="photoreports-detail__comments-item-text">
                                      <span class="orange"><?=$comment['AUTHOR']?>: </span>
                                      <?=$comment['MESSAGE']?>
                                  </p>
                              </div>
                          <?endforeach;?>
                      </div>
                  </div>
              </div>
          <? endif ?>
      <? endforeach; ?>
  </div>
</form>


