<?

use Bitrix\Main\Localization\Loc;
use Photo\Reports\Comments\Comment;
use Photo\Reports\Permission\RoleResolver;
use Photo\Reports\PhotoReportsService;
use Photo\Reports\Reports\Photo;
use Photo\Reports\Reports\Report;
use Photo\Reports\Reports\StatusEnum;
use Photo\Reports\Tasks\TaskOrientEnum;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);


class PhotoReportsElementComponent extends PhotoReportsComponent
{
    /**
     * выполняет логику работы компонента
     */
    public function executeComponent()
    {
        $this->setSefDefaultParams();
        global $APPLICATION,  $USER;


        /** @var PhotoReportsService $reportService */
        $reportService = $this->getContainer()
            ->get('photoreport_service');

        $this->resolvePage();


        try {

            if ($elementId = (int)$this->arParams['ELEMENT_ID']) {
                /** @var Report $report */
                $report = $reportService->getReportById($elementId,1);


                $statusEnum = new StatusEnum();

                $report->setStatus($statusEnum->getById($report->getUfStatus()));

                $this->arResult['DATA'] = $report->toArray(1);

                $this->arResult['DATA']['PHOTOS'] = $report->getPhotos();
                



                $arPhotos = [];
                    $first = $this->arResult["DATA"]['MARKET']['UF_R_CODE'];
                    $second = $this->arResult["DATA"]['NAME'];
                $APPLICATION->SetTitle(htmlspecialchars_decode($first.", ".$second));

                $this->arResult['DATA']['DEADLINE'] = $report->getTask()
                    ->getDeadline()
                    ->format('d.m.Y');

                $this->arResult['DATA']['CONTROL_DATE'] = ($report->getUfControlDate())
                    ? $report->getUfControlDate()->format('d.m.Y')
                    : null;

                $user = $USER->GetById($report->getTask()
                    ->getCreatedBy())
                    ->Fetch();

                $this->arResult['DATA']['USER_CREATE'] = implode(' ', [$user['NAME'],  $user['LAST_NAME']]);




                /** @var Photo $photo */
                foreach ($this->arResult["DATA"]['PHOTOS'] as $k => $photo) {
                    if ($photo->isActive()) {
                        $photo->init();
                        $comments = [];
                        /** @var Comment $c */
                        foreach ($photo->getComments() as $c) {
                            $comment =  $c->toArraySensitive(1);
                            $user = $reportService->getModuleUser($c->getUserCreate());
                            $comment['AUTHOR'] = $user->getFullName();
                            $comments[]  = $comment;
                        }
                        $arPhotos[] = [
                            'ID' => $photo->getId(),
                            'SRC' => $photo->getDetailFilePath(),
                            'DETAIL_URL' => $this->getReportPhotoDetail($elementId, $photo->getId()),
                            'ACTIVE' => $photo->isActive(),
                            'IS_APPROVE' => $photo->getIsApprove(),
                            'COMMENTS' => $comments,
                            'NUMBER' => $photo->getSort()

                        ];
                    }
                }

                $this->arResult['DATA']['PHOTOS'] = $arPhotos;

                $orientEnum = new TaskOrientEnum();
                $orientId = $this->arResult['DATA']['TASK']['ORIENTATION'];

                $this->arResult['DATA']['TASK']['ORIENTATION'] = $orientEnum->getById($orientId )['VALUE'];
                $this->arResult['DATA']['STATUS_IS_REWORK'] = $report->isRework();
                $this->arResult['DATA']['STATUS_IS_CHECK'] = $report->isCheck();
                $this->arResult['DATA']['STATUS_IS_NEW'] = $report->isNew();
                $this->arResult['DATA']['STATUS_TEXT'] = $this->resolveStatusText($this->arResult['DATA']['STATUS']);
                $this->arResult['STATUS_APPROVED'] = $report->isDone();

            } else {
                throw new NoReportSetException('No report set');
            }

            $this->resolveBreadcumbs($report);

            $this->includeComponentTemplate($this->page);
        } catch (Exception $e) {
            ShowError($e->getMessage());
        }
    }

    /**
     * @param Report $report
     */
    public function resolveBreadcumbs($report)
    {
        global  $APPLICATION;
        if ($this->resolveRole()== self::PAGE_MERCHANDISER) {

            $APPLICATION->AddChainItem($report->getTask()->getName(),
                $this->getTaskListUrl($report->getIblockSectionId()));


        }

        $APPLICATION->AddChainItem($report->getName(),
            $this->getReportDetailUrl($report->getId()));
    }


    public function getReportPhotoDetail($report, $photoId)
    {
        return $this->getUrlByAliasTemplate('report_photo',
            [
                '#ELEMENT_ID#' => $report,
                '#PHOTO_ID#' => $photoId
            ]);
    }




}

class NoReportSetException extends Exception
{

}
