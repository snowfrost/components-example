<?php


use Photo\Reports\Reports\PhotoRepository;
use Photo\Reports\Reports\Photo;

class PhotoRepositoryTest extends PHPUnit_Framework_TestCase
{
    public function testPhotoAdd()
    {
        global $DB;

        $DB->StartTransaction();

        $photo = 795982;
        $path = CFIle::GetPath($photo);

        $reportId = 32755;
        $p = new Photo($photo, $reportId, 'Example photo');

        $pr = CoreContainer::find('photo_repository');

        $r = $pr->flush($p);

        $this->assertNotNull($r->getId());

        

        $DB->Rollback();
    }

    public function testCreateReports()
    {

    }
}
