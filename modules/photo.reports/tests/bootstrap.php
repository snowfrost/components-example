<?php

$_SERVER['DOCUMENT_ROOT'] = __DIR__.'/../../../..';

define('NOT_CHECK_PERMISSIONS', true);
define('NO_AGENT_CHECK', true);

global $DBType, $DBDebug, $DBDebugToFile, $DBHost, $DBName, $DBLogin, $DBPassword, $DB;
require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';
$DB->Connect($DBHost, $DBName, $DBLogin, $DBPassword);
