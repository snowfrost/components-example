<?php


use Photo\Reports\PhotoReportsService;
use Photo\Reports\Reports\ReportRepository;
use Photo\Reports\Tasks\Task;
use Bitrix\Main\Type\DateTime;
use Photo\Reports\Tasks\TaskTypeEnum;


class TaskRepositoryTest extends PHPUnit_Framework_TestCase
{
    public function testCheckSections()
    {
        global $DB;
        /** @var ReportRepository $s */
        $s = CoreContainer::find('report_repository');

        $taskTypeEnum = new TaskTypeEnum();
        $type = $taskTypeEnum->getByXmlId(Task::TYPE_MONTHLY_ACTIONS);

        $DB->StartTransaction();

        $r = new Task(null, new DateTime('20.12.2019'), [
            1735
        ], $type['ID']);

        $rId = $s->createReportSection([
            'TASK_ID' => $r->getId(),
            'TYPE' => $type['XML_ID'],
            'TYPE_ID' => $type['ID'],
            'SECTION_NAME' => $r->getTaskName(),
        ]);

        $this->assertTrue($rId > 0);



        $type = $taskTypeEnum->getByXmlId(Task::TYPE_REQUEST);

        $r = new Task('Example report', new DateTime('20.12.2019'), [
            1735
        ], $type['ID']);


        $rId = $s->createReportSection([
            'TASK_ID' => $r->getId(),
            'TYPE' => $type['XML_ID'],
            'TYPE_ID' => $type['ID'],
            'SECTION_NAME' => $r->getTaskName(),
        ]);

        $this->assertTrue($rId > 0);

        $type = $taskTypeEnum->getByXmlId(Task::TYPE_MONTHLY);

        $r = new Task(null, new DateTime('20.12.2019'), [
            1735
        ], $type['ID']);

        $rId = $s->createReportSection([
            'TASK_ID' => $r->getId(),
            'TYPE' => $type['XML_ID'],
            'TYPE_ID' => $type['ID'],
            'SECTION_NAME' => $r->getTaskName(),
        ]);

        $this->assertTrue($rId > 0);


        $type = $taskTypeEnum->getByXmlId(Task::TYPE_WEEKLY);
        $r = new Task(null, new DateTime('20.12.2019'), [
            1735
        ], $type['ID']);

        $rId = $s->createReportSection([
            'TASK_ID' => $r->getId(),
            'TYPE' => $type['XML_ID'],
            'TYPE_ID' => $type['ID'],
            'SECTION_NAME' => $r->getTaskName(),
        ]);

        $this->assertTrue($rId > 0);

        $type = $taskTypeEnum->getByXmlId(Task::TYPE_WEEKLY_ANIMALS);
        $r = new Task(null, new DateTime('20.12.2019'), [
            1735
        ], $type['ID']);

        $rId = $s->createReportSection([
            'TASK_ID' => $r->getId(),
            'TYPE' => $type['XML_ID'],
            'TYPE_ID' => $type['ID'],
            'SECTION_NAME' => $r->getTaskName(),
        ]);

        $this->assertTrue($rId > 0);

        $DB->Rollback();

    }

    public function testSectionsCreate()
    {
        global $DB;
        $DB->StartTransaction();
        /** @var ReportRepository $s */
        $s = CoreContainer::find('report_repository');

        $r = $s->createSection([
            'CODE' => 'abc',
            'NAME' => 'Hello world']);
        $this->assertTrue($r->getId() > 0);
        $DB->Rollback();
    }


    public function testCreate()
    {

        global $DB;
        /** @var PhotoReportsService $s */
        $s = CoreContainer::find('photoreport_service');

        $taskTypeEnum = new TaskTypeEnum();
        $type = $taskTypeEnum->getByXmlId(Task::TYPE_REQUEST);

        $r = new Task('Example report', new DateTime('20.12.2019'), [1735], $type['ID']);

        $DB->StartTransaction();
        $s->registerTask($r, 1);

        $this->assertNotNull($r->getId());

        /** @var Task $task */
        $task = $s->getTaskById($r->getId());


        $this->assertNotNull($task->getId());

        $data = $s->getReportsByTaskId($task->getId());
        $this->assertTrue($data->count() > 0);

        $DB->Rollback();
    }
}
