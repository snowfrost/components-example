<?php


use Toolbox\Core\Logger\LoggerTrait;
use Photo\Reports\Permission\PermissionService;
use Photo\Reports\Permission\PermissionTrait;

class PermissionServiceTest extends PHPUnit_Framework_TestCase
{
    use PermissionTrait;
    use LoggerTrait;

    public function testPermissionAdd()
    {
        global $DB;
        $DB->StartTransaction();

        $this->addReportPermissionOnMarket('12312312312', '555');

        $DB->Rollback();
    }
}
