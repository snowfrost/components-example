<?php
$MESS['PR_WORKFLOW_ERROR_MIN_PHOTO_COUNT']= 'Не загружено минимальное количество фото.';
$MESS['PR_WORKFLOW_ERROR_ALL_PHOTO_APPROVED']= 'Не удалось вернуть на доработку, должно быть хотя бы одно фото с комментарием от мерчандайзера.';
$MESS['PR_WORKFLOW_ERROR_CHECK_STATUS_NO_APPROVED_PHOTOS']= 'Не удалось отправить отчет на проверку, есть фото с комментарием от мерчандайзера, которые нужно заменить.';
$MESS['PR_WORKFLOW_ERROR_HAS_NOT_APPROVED_PHOTO']= 'Не удалось утвердить отчет, есть непринятые фото.';
$MESS['PR_WORKFLOW_ERROR_NO_CONTROL_DATE']= 'Не удалось вернуть на доработку, не указана дата нового дедлайна.';