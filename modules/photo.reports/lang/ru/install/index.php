<?php


$MESS['PHOTO_CORE_MODULE_NAME'] = 'PhotoReports';
$MESS['PHOTO_CORE_MODULE_DESCRIPTION'] = 'PhotoReports - фото отчетов.';

$MESS['PHOTО_REPORT_IS_REPORT'] = 'Отчет';
$MESS['PHOTО_REPORT_MARKET'] = 'Магазин';
$MESS['PROPERTY_UF_CONTROL_DATE'] = 'Контрольная дата';
$MESS['PROPERTY_UF_DEADLINE'] = 'Дата окончания (дедлайн)';
$MESS['PROPERTY_UF_STATUS'] = 'Статус';
$MESS['PROPERTY_UF_TASK_ID'] = 'Задание';
$MESS['PROPERTY_UF_IS_TASK'] = 'Раздел задания';
$MESS['PROPERTY_UF_RATING'] = 'Рейтинг';
$MESS['REPORT_STATUS_NEW'] = 'Новый';

$MESS['PROPERTY_UF_REPORT_TYPE'] = 'Тип отчета';
$MESS['REPORT_TYPE_MONTHLY'] = 'Полный ежемесячный отчет';
$MESS['REPORT_TYPE_WEEKLY_ANIMALS'] = 'Еженедельный фотоотчет по живому товару';
$MESS['REPORT_TYPE_WEEKLY'] = 'Еженедельный отчет сезонный';
$MESS['REPORT_TYPE_REQUEST'] = 'Фотоотчеты по запросу';
$MESS['REPORT_TYPE_MONTHLY_ACTIONS'] = 'Ежемесячный фотоотчет "Акции"';


$MESS['TASKS_IBLOCK_NAME'] = 'Задания фотоотчетов';

$MESS['REPORT_STATUS_NEW'] = 'Новый';
$MESS['REPORT_STATUS_CHECK'] = 'Фотоотчет ждет проверки';
$MESS['REPORT_STATUS_REWORK'] = 'Фотоотчет отправлен на доработку';
$MESS['REPORT_STATUS_DONE'] = 'Фотоотчет утвержден с просмотром';
$MESS['REPORT_STATUS_DONE_WARNING'] = 'Фотоотчет утвержден с замечаниями';
$MESS['REPORT_STATUS_STATUS_DONE_NOT_VIEW'] = 'Фотоотчет утвержден без просмотра';


$MESS['INFO_PROPERTY_ADD'] = 'IBlock=#IBLOCK_ID#, property=#CODE# is add id=#ID#';
$MESS['INFO_PROPERTY_ADD_ERROR'] = 'IBlock=#IBLOCK_ID#, property=#CODE# add is error';
$MESS['INFO_PROPERTY_EXIST'] = 'IBlock=#IBLOCK_ID#, property=#CODE# is already exist id=#ID#';

