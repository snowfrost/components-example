<?php

$container = CoreContainer::getInstance();

$container->add('comment_repository', 'Photo\Reports\Comments\CommentRepository');
$container->add('event_repository', 'Photo\Reports\Events\EventRepository');
$container->add('report_repository', 'Photo\Reports\Reports\ReportRepository');
$container->add('photo_repository', 'Photo\Reports\Reports\PhotoRepository');
$container->add('task_repository', 'Photo\Reports\Tasks\TaskRepository');
$container->add('task_template_repository', 'Photo\Reports\TaskTemplates\TaskTemplateRepository');
$container->add('pr_permission_repository', '\Photo\Reports\Permission\PermissionRepository');
$container->add('pr_role_relation_repository', '\Photo\Reports\Permission\Role\RoleRelationRepository');
$container->add('pr_role_permission_repository', '\Photo\Reports\Permission\Role\RolePermissionRepository');
$container->add('pr_permission_service', '\Photo\Reports\Permission\PermissionService')
    ->withArgument('fp_permission_repository')
    ->withArgument('fp_role_relation_repository')
    ->withArgument('fp_role_permission_repository');

$container->add('photoreport_service', 'Photo\Reports\PhotoReportsService')
    ->withArgument('report_repository')
    ->withArgument('photo_repository')
    ->withArgument('comment_repository')
    ->withArgument('event_repository')
    ->withArgument('market_repository')
    ->withArgument('task_repository')
    ->withArgument('pr_status_workflow');

$container->add('pr_status_workflow', '\Photo\Reports\Reports\WorkFlow\StatusWorkflowRepository');
$container->add('pr_report_workflow_manager', '\Photo\Reports\Reports\WorkFlow\ReportsWorkFlowManager')
    ->withArgument('photoreport_service')
    ->withArgument('pr_status_workflow')
    ->withArgument('fp_structure_service')
    ->withArgument('fp_notify_service');
