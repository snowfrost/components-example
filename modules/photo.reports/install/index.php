<?php

/*
 * This file is part of the Studio Fact package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Toolbox\Core\Iblock\IBlockTrait;
use Toolbox\Core\Iblock\UserField;
use Toolbox\Core\Logger\LoggerTrait;

use Toolbox\Core\Permission\Role\RoleEntityRepository;
use Toolbox\Core\Permission\Role\RolePermissionRepository;
use Toolbox\Core\Permission\Role\RoleRelationRepository;
use Photo\Reports\Comments\CommentEntityTable;
use Photo\Reports\Events\EventEntityTable;
use Photo\Reports\ModuleConfig;
use Photo\Reports\Permission\PermissionEntityTable;
use Photo\Reports\Permission\Role\RoleEntityTable;
use Photo\Reports\Permission\Role\RolePermissionEntityTable;
use Photo\Reports\Permission\Role\RoleRelationEntityTable;
use Photo\Reports\Reports\WorkFlow\StatusWorkflowEntityTable;


Loc::loadMessages(__FILE__);


class photo_reports extends CModule
{
    use LoggerTrait;
    use IBlockTrait;
    /**
     * @var string
     */
    public $MODULE_ID = 'photo.reports';

    /**
     * @var string
     */
    public $MODULE_VERSION;

    /**
     * @var string
     */
    public $MODULE_VERSION_DATE;

    /**
     * @var string
     */
    public $MODULE_NAME;

    /**
     * @var string
     */
    public $MODULE_DESCRIPTION;

    /**
     * @var string
     */
    public $MODULE_PATH;

    /**
     * Construct object
     */
    public function __construct()
    {
        $this->MODULE_NAME = Loc::getMessage('PHOTO_CORE_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PHOTO_CORE_MODULE_DESCRIPTION');
        $this->MODULE_PATH = $this->getModulePath();

        $arModuleVersion = array();
        include $this->MODULE_PATH . '/install/version.php';
        if (!class_exists('Photo\Reports\ModuleConfig')) {
            include $this->MODULE_PATH . '/lib/moduleconfig.php';
            include $this->MODULE_PATH . '/lib/events/evententity.php';
            include $this->MODULE_PATH . '/lib/comments/commententity.php';
            include $this->MODULE_PATH . '/lib/reports/workflow/statusworkflowentity.php';
            include $this->MODULE_PATH . '/lib/permission/role/roleentity.php';
            include $this->MODULE_PATH . '/lib/permission/role/rolepermissionentity.php';
            include $this->MODULE_PATH . '/lib/permission/role/rolerelationentity.php';
            include $this->MODULE_PATH . '/lib/permission/permissionentity.php';
            include $this->MODULE_PATH . '/lib/permission/permissionresolver.php';
        }


        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
    }

    /**
     * Return path module
     *
     * @return string
     */
    protected function getModulePath()
    {
        $modulePath = explode('/', __FILE__);
        $modulePath = array_slice($modulePath, 0, array_search($this->MODULE_ID, $modulePath) + 1);

        return join('/', $modulePath);
    }

    /**
     * Return components path for install
     *
     * @param  bool $absolute
     * @return string
     */
    protected function getComponentsPath($absolute = true)
    {
        $documentRoot = getenv('DOCUMENT_ROOT');
        if (strpos($this->MODULE_PATH, 'local/modules') !== false) {
            $componentsPath = '/local/components';
        } else {
            $componentsPath = '/bitrix/components';
        }

        if ($absolute) {
            $componentsPath = sprintf('%s%s', $documentRoot, $componentsPath);
        }

        return $componentsPath;
    }

    /**
     * Install module
     * @return void
     * @throws Exception
     */
    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);

        if (!CModule::IncludeModule('tooolbox.core')) {
            throw new Exception('Please include module toolbox.core');
        }

        $this->installDB();
        $this->installFiles();
        $this->installEvents();
    }

    /**
     * Remove module
     *
     * @return void
     */
    public function doUninstall()
    {
        $this->unInstallDB();
        $this->unInstallFiles();
        $this->unInstallEvents();

        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    /**
     * Add tables to the database
     *
     * @return bool
     */
    public function installDB()
    {

        return false;
    }

    /**
     * Remove tables from the database
     *
     * @return bool
     */
    public function unInstallDB()
    {


        return true;
    }

    public function ratingVal()
    {
        $vals = [];
        for ($i = 1; $i <= 10; $i++) {
            $vals[] = [
                'XML_ID' => 'rating-' . $i,
                'VALUE' => $i,
                'DEF' => 'Y',
                'SORT' => 100 * $i
            ];
        }

        self::log('Rating vals', $vals);

        return $vals;
    }

    /**
     * Add post events
     *
     * @return bool
     */
    public function installEvents()
    {
        return true;
    }

    /**
     * Delete post events
     *
     * @return bool
     */
    public function unInstallEvents()
    {
        return true;
    }

    /**
     * Copy files module
     *
     * @return bool
     */
    public function installFiles()
    {
        return true;
    }

    /**
     * Remove files module
     *
     * @return bool
     */
    public function unInstallFiles()
    {
        return true;
    }

    public function createMailTemplate()
    {
            $et = new CEventType;
            if ($id =  $et->Add([
                "LID"           => 'ru',
                "EVENT_NAME"    => 'REPORT_STATUS_NOTIFICATION',
                "NAME"          => 'Изменился статус отчета',
                "DESCRIPTION"   => "
                #TEXT# - Текст сообщения
                "
            ])){

                $arr["ACTIVE"]      = "Y";
                $arr["EVENT_NAME"]  = "REPORT_STATUS_NOTIFICATION";
                $arr["LID"]         = "s1";
                $arr["EMAIL_FROM"]  = "admin@site.ru";
                $arr["EMAIL_TO"]    = "admin@site.ru";
                $arr["BCC"]         = "";
                $arr["SUBJECT"]     = "Изменен статус отчета #ID#";
                $arr["BODY_TYPE"]   = "text";
                $arr["MESSAGE"]     = "
    Внимание! Статус отчет  #ID# изменен.
    Новый статус- #STATUS#
    ";
                $obTemplate = new CEventMessage;
                if ($id = $obTemplate->Add($arr)){
                    echo 'Template id=' . $id;
                } else {
                    echo 'Template create error: ' . $obTemplate->LAST_ERROR;
                }

                echo 'Created ' .$id;
            } else {
                echo 'Not created';
            }

    }


}

