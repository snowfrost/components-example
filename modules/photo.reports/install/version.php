<?php

/*
 * This file is part of the Studio Fact package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

$arModuleVersion = array(
    'VERSION' => '0.0.1-DEV',
    'VERSION_DATE' => '',
);
