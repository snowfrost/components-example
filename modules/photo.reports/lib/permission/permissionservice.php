<?php

namespace Photo\Reports\Permission;

use Toolbox\Core\Permission\Role\RoleType;
use Toolbox\Core\User\User;
use Photo\Reports\ModuleConfig;
use \Toolbox\Core\Permission\PermissionService as CommonPermissionService;


class PermissionService extends CommonPermissionService
{
    protected $moduleId = ModuleConfig::MODULE_ID;

    /**
     * @param User $user
     */
    public function fillUserPermissions(User $user)
    {
        $rights = $this->getUserRights($user);
        $user->setRights($this->moduleId, $rights);

        if (RoleResolver::userIsMerchandiser($user)) {
            $attrs = RoleType::resolveAttr($user);
        } elseif (RoleResolver::userIsMarket($user)) {
            $attrs = RoleType::resolveDepartmentAttrs($user->getUfDepartment());
        } else {
            $attrs =  [];
        }


        $user->setAttrs($attrs);
    }


}