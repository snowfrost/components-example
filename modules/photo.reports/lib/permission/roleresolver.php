<?php


namespace Photo\Reports\Permission;


use Toolbox\Core\User\User;
use Photo\Reports\ModuleConfig;
use Photo\Reports\PhotoReportsService;

class RoleResolver
{
    const ADMIN = 'ADMINS';
    const MANAGER = 'MANAGERS';
    const MARKET = 'MARKETS';
    const MERCHANDISERS = 'MERCHANDISERS';

    const POSITION_MTZ = 'Менеджер торгового зала';
    const POSITION_UM = 'Управляющий магазином';

    public static function userIsMerchandiser(User $user)
    {

        foreach ($user->getRoles(ModuleConfig::MODULE_ID) as $role){
            if ($role['R_CODE'] == self::MERCHANDISERS){
                return true;
            }
        }



        return false;
    }

    public static  function userIsMarket(User $user)
    {
        $acceptedPositions = [
            self::POSITION_MTZ,
            self::POSITION_UM
        ];

        if (in_array($user->getWorkPosition(), $acceptedPositions)) {
            foreach ($user->getRoles(ModuleConfig::MODULE_ID) as $role){
                if ($role['R_CODE'] == self::MARKET){
                    return true;
                }
            }
        }

        return false;
    }

    public static  function userIsAdmin(User $user)
    {
        foreach ($user->getRoles(ModuleConfig::MODULE_ID) as $role){
            if ($role['R_CODE'] == self::ADMIN){
                return true;
            }
        }

        return false;
    }
}