<?php


namespace Photo\Reports\Permission;


use Toolbox\Core\Permission\PermissionRepository;
use Toolbox\Core\Permission\Role\Role;
use Toolbox\Core\Permission\Role\RoleType;
use Photo\Reports\Entities;


trait PermissionTrait
{
    public function addReportPermission(Role $role, $entityId = null)
    {
        /** @var PermissionService $permService */
        $permService = \CoreContainer::getInstance()
            ->find('pr_permission_service');

        $permService->addPermission($entityId,
            Entities::REPORT,
            $role->getRoleId(),
            $permService->getModuleId());
    }

    public function registerReportPermissionOnDepartment($entity, $department)
    {
        /** @var PermissionService $permService */
        $permService = \CoreContainer::getInstance()
            ->find('pr_permission_service');

        $r = $permService->addPermission($entity,
            Entities::REPORT,
            RoleType::resolveId($department, RoleType::DEPARTMENT),
            $permService->getModuleId());

        if ($r->isSuccess()) {
            self::log('Add perm for report ' . $entity . ' by marketId ' . $department);
        } else {
            self::error('Error add perm ', $r->getErrorMessages());
        }
    }

    public function registerTaskPermissionOnDepartment($entity, $departmentId)
    {
        /** @var PermissionService $permService */
        $permService = \CoreContainer::getInstance()
            ->find('pr_permission_service');

        $r = $permService->addPermission($entity,
            Entities::TASK,
            RoleType::resolveId($departmentId, RoleType::DEPARTMENT),
            $permService->getModuleId());

        if ($r->isSuccess()) {
            self::log('Add perm for task ' . $entity . ' by marketId ' . $departmentId);
        } else {
            self::error('Error add perm ', $r->getErrorMessages());
        }
    }

    public function getEntities($type, array $attrs)
    {
        /** @var PermissionService $permService */
        $permService = \CoreContainer::getInstance()
            ->find('pr_permission_service');
        /** @var PermissionRepository $permRepository */
        $permRepository = \CoreContainer::getInstance()
            ->find('fp_permission_repository');

        return  $permRepository->getEntities($type, $attrs, $permService->getModuleId());
    }
}