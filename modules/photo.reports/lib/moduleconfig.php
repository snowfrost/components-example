<?php


namespace Photo\Reports;


class ModuleConfig
{
    const MODULE_ID = 'photo.reports';

    const IBLOCK_TYPE = 'photos';
    const IBLOCK_CODE = 'photo_reports';
    const IBLOCK_NAME = 'Фото отчеты';

    const GROUPS_MARKETS_DIR = 'g_department_manager_only';
    const GROUPS_MARKETS_SUB_DIR = 'g_department_manager';

    const PROPERTY_PHOTO_IS_APPROVE = 'IS_APPROVE';
    const PROPERTY_PHOTO_PARENT_ID = 'PARENT_ID';
    const PROPERTY_PHOTO_ORIENTATION = 'ORIENTATION';

    const PROPERTY_UF_REPORT = 'UF_REPORT';
    const PROPERTY_UF_MARKET = 'UF_MARKET';
    const PROPERTY_UF_IS_TASK = 'UF_IS_TASK';
    const PROPERTY_UF_STATUS = 'UF_STATUS';

    const STATUS_NEW = 'pr_new';
    const STATUS_CHECK = 'pr_check';
    const STATUS_REWORK = 'pr_rework';
    const STATUS_DONE = 'pr_done';
    const STATUS_DONE_WARNING = 'pr_done_warning';
    const STATUS_DONE_NOT_VIEW = 'pr_done_not_view';

    const MARKET_STATUS_DONE = "Фотоотчет утвержден";

    const PROPERTY_UF_TASK_ID = 'UF_TASK_ID';
    const PROPERTY_UF_DEADLINE = 'UF_DEADLINE';
    const PROPERTY_UF_CONTROL_DATE = 'UF_CONTROL_DATE';
    const PROPERTY_UF_RATING = 'UF_RATING';
    const PROPERTY_UF_REPORT_TYPE = 'UF_REPORT_TYPE';

    const TYPE_MONTHLY = 'pr_monthly';
    const TYPE_WEEKLY_ANIMALS = 'pr_weekly_animals';
    const TYPE_WEEKLY = 'pr_weekly';
    const TYPE_REQUEST = 'pr_request';
    const TYPE_MONTHLY_ACTIONS = 'pr_monthly_actions';



    const PROPERTY_DEADLINE = 'DEADLINE';
    const PROPERTY_REPORT_TYPE = 'REPORT_TYPE';
    const PROPERTY_EXAMPLES = 'EXAMPLES';
    const PROPERTY_MARKETS = 'MARKETS';
    const PROPERTY_CITIES = 'CITIES';
    const PROPERTY_ORIENTATION = 'ORIENTATION';
    const PROPERTY_MIN_PHOTO_COUNT = 'MIN_PHOTO_COUNT';
    const PROPERTY_SD_SERVICE = 'SD_SERVICE';
    const PROPERTY_SD_CATEGORY = 'SD_CATEGORY';
    const PROPERTY_IS_TEMPLATE = 'IS_TEMPLATE';







    const TASK_IBLOCK_TYPE = 'photos';
    const TASK_IBLOCK_CODE = 'reports_tasks';

    const MAX_REWORK_COUNT = 2;

}