<?php


namespace Photo\Reports\TaskTemplates;


use Toolbox\Core\Repository\TableElementInterface;
use Toolbox\Core\Util\ArrayTrait;
use Toolbox\Core\Util\Lang;

class TaskTemplate implements TableElementInterface
{
    use ArrayTrait;
    use Lang;

    private $id;
    private $name;
    private $serialized_form;
    private $file_id;
    private $create_at;
    private $update_at;
    private $user_update;
    private $user_create;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return TaskTemplate
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


    /**
     * @param $name
     * @return TaskTemplate
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSerializedForm()
    {
        return $this->serialized_form;
    }

    /**
     * @param $serialized_form
     * @return TaskTemplate
     */

    public function setSerializedForm(array $serialized_form)
    {
        if (is_array($serialized_form)){
            $this->serialized_form = serialize($serialized_form);
        } else {
            $this->serialized_form = $serialized_form;
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileId()
    {
        return $this->file_id;
    }

    /**
     * @param $file_id
     * @return TaskTemplate
     */
    public function setFileId($file_id)
    {
        if (is_array($file_id)){
            $this->file_id = serialize($file_id);
        } else {
            $this->file_id = $file_id;
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param mixed $create_at
     * @return TaskTemplate
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param mixed $update_at
     * @return TaskTemplate
     */
    public function setUpdateAt($update_at)
    {
        $this->update_at = $update_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserUpdate()
    {
        return $this->user_update;
    }

    /**
     * @param mixed $user_update
     * @return TaskTemplate
     */
    public function setUserUpdate($user_update)
    {
        $this->user_update = $user_update;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserCreate()
    {
        return $this->user_create;
    }

    /**
     * @param mixed $user_create
     * @return TaskTemplate
     */
    public function setUserCreate($user_create)
    {
        $this->user_create = $user_create;
        return $this;
    }
}