<?php


namespace Photo\Reports\TaskTemplates;


use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\DatetimeField;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Type\DateTime;

class TaskTemplateEntityTable extends DataManager
{
    /**
     * @return null
     */
    public static function getTableName()
    {
        return 'photo_reports_task_templates';
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
                'unique' => true
            ]),
            new StringField('NAME',[
                'required'=>true
            ]),
            new StringField('SERIALIZED_FORM'),
            new StringField('FILE_ID'),
            new DatetimeField('CREATE_AT', [
                'default_value' => new DateTime()
            ]),
            new DatetimeField('UPDATE_AT', [
                'default_value' => new DateTime()
            ]),
            new StringField('USER_CREATE'),
            new StringField('USER_UPDATE'),
        ];
    }


}