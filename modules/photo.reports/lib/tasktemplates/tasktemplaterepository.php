<?php


namespace Photo\Reports\TaskTemplates;

use Toolbox\Core\Repository\Result;
use Toolbox\Core\Repository\TableRepository;
use Photo\Reports\Events\EventTrait;
use Photo\Reports\TaskTemplates\TaskTemplateEntityTable;

class TaskTemplateRepository extends TableRepository
{
    use EventTrait;

    /**
     * @return TaskTemplateEntityTable
     */


    public function saveAndSetFileId(TaskTemplate $taskTemplate ,$arFiles)
    {
        $fileIds = [];
        foreach ($arFiles as $arFile) {
            $imageId = \CFile::SaveFile($arFile, false);
            if ($imageId) {
                $fileIds[] = $imageId;
            }
        }
        $taskTemplate->setFileId($fileIds);
    }

    public function getEntity()
    {
        return new TaskTemplateEntityTable();
    }

    /**
     * @param array $filter
     * @param array $select
     * @return Result
     */



    public function getTaskTemplatesList($filter = array(), $select = array())
    {
        $parameters = ['order' => ['ID' => 'ASC']];
        if ($filter) {
            $parameters['filter'] = $filter;
        }
        if ($select) {
            $parameters['select'] = $select;
        }
        $dbTaskTemplate = $this->getEntity()
            ->getList($parameters);
        $result = [];

        while ($obTaskTemplate = $dbTaskTemplate->fetch()) {
            $item = [];
            foreach ($obTaskTemplate as $key => $field) {
                switch ($key) {
                    case 'FILE_ID':
                    case 'SERIALIZED_FORM':
                        $item[$key] = unserialize($field);
                        break;
                    default:
                        $item[$key] = $field;
                }
            }
            $result[] = $item;
        }
        return new Result($result);
    }

    public function save(array $data)
    {
        if ($e = $this->getEntity()
            ->getList(
                [
                    'filter' => [
                        'ID' => $data['ID'],
                    ]
                ])
            ->fetch()) {

            return $this->getEntity()
                ->update($e['ID'], $data);

        } else {
            return $this->getEntity()
                ->add($data);
        }
    }
}
