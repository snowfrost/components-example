<?php


namespace Photo\Reports\Events;


use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\DatetimeField;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Type\DateTime;

class EventEntityTable extends DataManager
{
    /**
     * @return null
     */
    public static function getTableName()
    {
       return 'photo_reports_events';
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
                'unique' => true
            ]),
            new DatetimeField('CREATE_AT', [
                'default_value' => new DateTime()
            ]),
            new StringField('ENTITY'),
            new StringField('ENTITY_TYPE'),
            new StringField('MESSAGE'),
            new StringField('USER_CREATE'),
        ];
    }

}