<?php


namespace Photo\Reports\Events;


use Bitrix\Main\Type\Date;
use Bitrix\Main\Type\DateTime;

trait EventTrait
{

    /** @return EventRepository */
    public function getEventRepository()
    {
        return \CoreContainer::find('event_repository');
    }

    public function saveEvent(array $data)
    {
       return $this->getEventRepository()->save($data);
    }

    public function addTaskEvent($id)
    {
        global $USER;
        $this->saveEvent([
            'CREATE_AT' => new DateTime(),
            'ENTITY' => $id,
            'USER_CREATE' => $USER->GetId(),
            'ENTITY_TYPE' => Event::TYPE_TASK,
            'MESSAGE' => Event::METHOD_CREATE,
        ]);
    }

    public function addReportEvent($id)
    {
        global $USER;
        $this->saveEvent([
            'CREATE_AT' => new DateTime(),
            'ENTITY' => $id,
            'USER_CREATE' => $USER->GetId(),
            'ENTITY_TYPE' => Event::TYPE_REPORT,
            'MESSAGE' => Event::METHOD_CREATE,
        ]);
    }

    public function addPhotoEvent($id)
    {
        global $USER;
        $this->saveEvent([
            'CREATE_AT' => new DateTime(),
            'ENTITY' => $id,
            'USER_CREATE' => $USER->GetId(),
            'ENTITY_TYPE' => Event::TYPE_PHOTO,
            'MESSAGE' => Event::METHOD_CREATE,
        ]);
    }

    public function updatePhotoEvent($id)
    {
        global $USER;
        $this->saveEvent([
            'CREATE_AT' => new DateTime(),
            'ENTITY' => $id,
            'USER_CREATE' => $USER->GetId(),
            'ENTITY_TYPE' => Event::TYPE_PHOTO,
            'MESSAGE' => Event::METHOD_UPDATE,
        ]);
    }

    public function addCommentEvent($id)
    {
        global $USER;
        $this->saveEvent([
            'CREATE_AT' => new DateTime(),
            'ENTITY' => $id,
            'USER_CREATE' => $USER->GetId(),
            'ENTITY_TYPE' => Event::TYPE_COMMENT,
            'MESSAGE' => Event::METHOD_CREATE,
        ]);
    }

    public function changeReportStatus($reportId, $statusCode)
    {
        global $USER;
        return $this->saveEvent([
            'CREATE_AT' => new DateTime(),
            'ENTITY' => $reportId,
            'USER_CREATE' => $USER->GetId(),
            'ENTITY_TYPE' => Event::TYPE_REPORT,
            'MESSAGE' => $statusCode,
        ]);
    }

}