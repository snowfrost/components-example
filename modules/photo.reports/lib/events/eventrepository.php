<?php


namespace Photo\Reports\Events;


use Toolbox\Core\Repository\BaseRepository;

class EventRepository extends BaseRepository
{
    /**
     * @return mixed
     */
    public function getEntity()
    {
        return new EventEntityTable();
    }
}