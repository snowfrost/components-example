<?php


namespace Photo\Reports;


class Entities
{
    const REPORT = 'REPORT';
    const PHOTO = 'PHOTO';
    const TASK = 'TASK';
    const COMMENT = 'COMMENT';
    const TASK_TEMPLATE = 'TASK_TEMPLATE';
}