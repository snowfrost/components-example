<?php


namespace Photo\Reports\Tasks;


use Toolbox\Core\Iblock\IBlockTrait;
use Toolbox\Core\Repository\ElementEntity;
use Toolbox\Core\Repository\Enum\ListEnum;
use Photo\Reports\ModuleConfig;

class TaskEntityTable extends ElementEntity
{
    use IBlockTrait;

    const IBLOCK_CODE = ModuleConfig::TASK_IBLOCK_CODE;
    const IBLOCK_TYPE = ModuleConfig::TASK_IBLOCK_TYPE;

}