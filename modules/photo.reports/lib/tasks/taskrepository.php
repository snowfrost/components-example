<?php


namespace Photo\Reports\Tasks;


use Toolbox\Core\Logger\LoggerTrait;
use Toolbox\Core\Repository\BaseRepository;
use Toolbox\Core\Repository\IblockElementRepository;
use Photo\Reports\Events\EventTrait;

class TaskRepository extends IblockElementRepository
{
    use LoggerTrait;
    use EventTrait;
    /**
     * @return TaskEntityTable
     */
    public function getEntity()
    {
        return new TaskEntityTable();
    }


    public function flush(Task $task)
    {
        $result = parent::flush($task);

        if ($result->isSuccess()){
            $this->addReportEvent($task->getId());
        }

        return $result;
    }


}