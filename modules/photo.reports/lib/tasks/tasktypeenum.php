<?php


namespace Photo\Reports\Tasks;


use Toolbox\Core\Repository\Enum\ListEnum;
use Photo\Reports\ModuleConfig;

class TaskTypeEnum extends ListEnum
{
    const IBLOCK_CODE = ModuleConfig::TASK_IBLOCK_CODE;
    const IBLOCK_TYPE = ModuleConfig::TASK_IBLOCK_TYPE;
    const CODE = 'REPORT_TYPE';
}