<?php


namespace Photo\Reports\Tasks;


use Bitrix\Main\DB\Exception;
use Bitrix\Main\Type\DateTime;
use Toolbox\Core\Repository\ElementModel;
use Toolbox\Core\Util\ArrayTrait;
use Toolbox\Core\Util\Lang;
use Photo\Reports\ModuleConfig;
use Photo\Reports\Reports\Photo;

class Task extends ElementModel
{
    use ArrayTrait {
        ArrayTrait::toArray as protected toArr;
    }
    use Lang;

    const TYPE_MONTHLY = ModuleConfig::TYPE_MONTHLY;
    const TYPE_WEEKLY = ModuleConfig::TYPE_WEEKLY;
    const TYPE_WEEKLY_ANIMALS = ModuleConfig::TYPE_WEEKLY_ANIMALS;
    const TYPE_MONTHLY_ACTIONS = ModuleConfig::TYPE_MONTHLY_ACTIONS;
    const TYPE_REQUEST = ModuleConfig::TYPE_REQUEST;


    /** @var \Bitrix\Main\Type\DateTime */
    private $deadline;
    private $examples = [];
    private $cities = [];
    private $orientation;
    private $minPhotoCount;
    private $markets = [];

    private $reportType;
    private $sdService;
    private $sdCategory;

    /**
     * Task constructor.
     * @param DateTime $deadline
     * @param array $markets
     * @param $reportType
     * @param $id
     * @param string $name
     * @param string $previewText
     * @param string $orientation
     * @param int $minPhotoCount
     */
    public function __construct(
        $name = null,
        DateTime $deadline,
        array $markets,
        $reportType,
        $previewText = null,
        $orientation = null,
        $minPhotoCount = 5,
        $id = null
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->previewText = $previewText;
        $this->deadline = $deadline;
        $this->orientation = $orientation;
        $this->minPhotoCount = $minPhotoCount;
        $this->markets = $markets;
        $this->dateCreate = new DateTime();
        $this->timestampX = new DateTime();
        $this->reportType = $reportType;
    }


    /**
     * @return DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param DateTime $deadline
     * @return Task
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
        return $this;
    }

    /**
     * @return array
     */
    public function getExamples()
    {
        return $this->examples;
    }

    /**
     * @param array $examples
     * @return Task
     */
    public function setExamples($examples)
    {
        $this->examples = $examples;
        return $this;
    }

    /**
     * @return array
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param array $cities
     * @return Task
     */
    public function setCities($cities)
    {
        $this->cities = $cities;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrientation()
    {
        return $this->orientation;
    }

    /**
     * @param string $orientation
     * @return Task
     */
    public function setOrientation($orientation)
    {
        $this->orientation = $orientation;
        return $this;
    }

    /**
     * @return int
     */
    public function getMinPhotoCount()
    {
        return $this->minPhotoCount;
    }

    /**
     * @param int $minPhotoCount
     * @return Task
     */
    public function setMinPhotoCount($minPhotoCount)
    {
        $this->minPhotoCount = $minPhotoCount;
        return $this;
    }

    /**
     * @return array
     */
    public function getMarkets()
    {
        return $this->markets;
    }

    /**
     * @param array $markets
     * @return Task
     */
    public function setMarkets(array $markets)
    {
        $this->markets = $markets;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSdService()
    {
        return $this->sdService;
    }

    /**
     * @param mixed $sdService
     * @return Task
     */
    public function setSdService($sdService)
    {
        $this->sdService = $sdService;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSdCategory()
    {
        return $this->sdCategory;
    }

    /**
     * @param mixed $sdCategory
     * @return Task
     */
    public function setSdCategory($sdCategory)
    {
        $this->sdCategory = $sdCategory;
        return $this;
    }

    /**
     * @return null
     */
    public function getReportType()
    {
        return $this->reportType;
    }

    /**
     * @param null $reportType
     * @return Task
     */
    public function setReportType($reportType)
    {
        $this->reportType = $reportType;
        return $this;
    }

    public function getTaskName()
    {
        $reportType = new TaskTypeEnum();
        $type = $reportType->getById($this->getReportType());

        switch ($type['XML_ID']) {
            case Task::TYPE_MONTHLY:
            case Task::TYPE_MONTHLY_ACTIONS:
                return FormatDate('f',
                    $this->getDateCreate()
                        ->getTimestamp());
            case Task::TYPE_WEEKLY:
            case Task::TYPE_WEEKLY_ANIMALS:

                return FormatDate('f',
                        $this->getDateCreate()
                            ->getTimestamp()) . '-' . $this->getDateCreate()
                        ->format('W');

            case Task::TYPE_REQUEST:
                if (is_null($this->name)){
                    throw new Exception('Not name set for report by type');
                }
                return $this->getName();
            default:
                throw new \Exception('Not exist xml-id ' . $type['XML_ID'] . ' - report type ' . $this->getReportType());
        }
    }

    public function toArray($upper = false)
    {
        $result = [];
        $data = $this->toArr();
        foreach ($data as $k => $v) {

            if (is_object($v) && method_exists($v, 'toArray') ) {
                $v = $v->toArray(1);
            }

            if ($upper) {
                $result[strtoupper(self::toUnderscore($k))] =  $v;
            } else {
                $result[self::toUnderscore($k)] =  $v;
            }
        }

        return $result;

    }
}