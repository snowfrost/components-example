<?php


namespace Photo\Reports\Types;


use Toolbox\Core\Repository\Enum\UserFieldEnum;

class ReportTypeEnum extends UserFieldEnum
{
    const CODE = 'UF_REPORT_TYPE';
}