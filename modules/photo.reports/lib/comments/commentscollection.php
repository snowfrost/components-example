<?php


namespace Photo\Reports\Comments;


use Toolbox\Core\Collection\Collection;
use Toolbox\Core\User\User;
use Toolbox\Core\User\UserTrait;
use Photo\Reports\Permission\RoleResolver;
use Photo\Reports\PhotoReportsService;

class CommentsCollection extends Collection
{
    use UserTrait;


    public function hasManagerComments()
    {
        /** @var PhotoReportsService $photoReportService */
        $photoReportService = \CoreContainer::find('photoreport_service');
        /** @var Comment $lastComment */
        if ($lastComment = $this->last()){
            $userId = $lastComment->getUserCreate();
            /** @var User $user */
            $user = $photoReportService->getModuleUser($userId);
            if (RoleResolver::userIsMerchandiser($user) || RoleResolver::userIsAdmin($user)) {
                return true;
            }
        }
        return false;
    }
}