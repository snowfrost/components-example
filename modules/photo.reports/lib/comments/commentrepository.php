<?php


namespace Photo\Reports\Comments;


use Toolbox\Core\Repository\BaseRepository;
use Toolbox\Core\Repository\Result;
use Photo\Reports\Events\EventTrait;

class CommentRepository extends BaseRepository
{
    use EventTrait;

    /**
     * @return CommentEntityTable
     */
    public function getEntity()
    {
        return new CommentEntityTable();
    }

    public function getReportComments($id = null)
    {
        $data = $this->getEntity()
            ->getList([
                'filter' => [
                    'ENTITY' => $id,
                    'ENTITY_TYPE' => Comment::ENTITY_TYPE_REPORT
                ],
                'order' => ['ID' => 'ASC']
            ])
            ->fetchAll();

        $data = $this->mapResult($data);

        return new Result($data);
    }

    public function getPhotoComments($id = null)
    {
        $data = $this->getEntity()
            ->getList([
                'filter' => [
                    'ENTITY' => $id,
                    'ENTITY_TYPE' => Comment::ENTITY_TYPE_PHOTO
                ],
                'order' => ['ID' => 'ASC']
            ])
            ->fetchAll();

        $data = $this->mapResult($data);

        return new Result($data);
    }

    public function getComments($id, $entityType)
    {
        $data = new Result();

        if($entityType === Comment::ENTITY_TYPE_REPORT) {
            $data = $this->getReportComments($id);
        } else if($entityType === Comment::ENTITY_TYPE_PHOTO) {
            $data = $this->getPhotoComments($id);
        }

        return $data;
    }

    public function flush(Comment $comment)
    {
        $result = new Result();

        $data = $this->prepareFields($comment->toArray());

        if ($id = $this->save($data)){
            $comment->setId($id);
            $result->setId($id)->setData([$comment]);
            $this->addCommentEvent($id);
        }

        return $result;
    }
}
