<?php


namespace Photo\Reports\Comments;


use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\DatetimeField;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Type\DateTime;

class CommentEntityTable extends DataManager
{
    /**
     * @return null
     */
    public static function getTableName()
    {
        return 'photo_reports_comments';
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
                'unique' => true
            ]),
            new StringField('ENTITY_TYPE'),
            new StringField('ENTITY'),
            new StringField('MESSAGE'),
            new DatetimeField('CREATE_AT', [
                'default_value' => new DateTime()
            ]),
            new DatetimeField('UPDATE_AT', [
                'default_value' => new DateTime()
            ]),
            new StringField('USER_CREATE'),
            new StringField('USER_UPDATE'),
        ];
    }


}