<?php


namespace Photo\Reports\Comments;


use Toolbox\Core\Util\ArrayTrait;
use Toolbox\Core\Util\Lang;

class Comment
{
    use ArrayTrait;
    use Lang;
    const ENTITY_TYPE_REPORT = 'report';
    const ENTITY_TYPE_PHOTO = 'photo';

    private $id;
    private $entity_type;
    private $entity;
    private $message;
    private $create_at;
    private $update_at;
    private $user_update;
    private $user_create;
    private $user;
    private $userName;

    

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Comment
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntityType()
    {
        return $this->entity_type;
    }

    /**
     * @param mixed $entity_type
     * @return Comment
     */
    public function setEntityType($entity_type)
    {
        $this->entity_type = $entity_type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param mixed $entity
     * @return Comment
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     * @return Comment
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreateAt()
    {
        return $this->create_at;
    }

    /**
     * @param mixed $create_at
     * @return Comment
     */
    public function setCreateAt($create_at)
    {
        $this->create_at = $create_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdateAt()
    {
        return $this->update_at;
    }

    /**
     * @param mixed $update_at
     * @return Comment
     */
    public function setUpdateAt($update_at)
    {
        $this->update_at = $update_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserUpdate()
    {
        return $this->user_update;
    }

    /**
     * @param mixed $user_update
     * @return Comment
     */
    public function setUserUpdate($user_update)
    {
        $this->user_update = $user_update;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserCreate()
    {
        return $this->user_create;
    }

    /**
     * @param mixed $user_create
     * @return Comment
     */
    public function setUserCreate($user_create)
    {
        $this->user_create = $user_create;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     * @return Comment
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
        return $this;
    }


}