<?php


namespace Photo\Reports\Reports\WorkFlow;


class ReportWorkFlow
{
    private $statuses = [
        [
            'name' => 'pr_new',
            'transitions' => [
                'pr_check',
                'pr_done_not_view',
            ],
            'rolesCanSet' => [],
            'rolesCanView' => [],
            'isStart' => true
        ],
        [
            'name' => 'pr_check',
            'transitions' => [
                'pr_done_warning',
                'pr_done',
                'pr_rework',
            ],
        ],
        [
            'name' => 'pr_rework',
            'transitions' => [
                'pr_check',
            ],
            'conditions' => [

            ],
        ],
        [
            'name' => 'pr_done_warning',
            'transitions' => [
            ],
            'isFinish' => true
        ],
        [
            'name' => 'pr_done_not_view',
            'transitions' => [
            ],
            'isFinish' => true
        ],
        [
            'name' => 'pr_done',
            'transitions' => [
            ],
            'isFinish' => true
        ],
    ];

    public function buildGraph()
    {
        foreach ($this->statuses as $status) {
            $reflector = new \ReflectionClass(ReportStatus::class);
            $status = $reflector->newInstanceWithoutConstructor();


        }
    }
}