<?php


namespace Photo\Reports\Reports\WorkFlow;


use Photo\Reports\Reports\Report;

trait WorkFlowTrait
{
    /** @return ReportsWorkFlowManager */
    public static function getWorkflowManager()
    {
        return \CoreContainer::find('pr_report_workflow_manager');
    }

    public static function setNewStatus(Report $report)
    {
        self::getWorkflowManager()->changeStatus($report, Report::STATUS_NEW);
    }
}