<?php


namespace Photo\Reports\Reports\WorkFlow;


use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\DatetimeField;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Type\DateTime;

class StatusWorkflowEntityTable extends DataManager
{

    /**
     * @return null
     */
    public static function getTableName()
    {
        return 'pr_status_workflow';
    }

    /**
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
                'unique' => true
            ]),
            new DatetimeField('CREATE_AT', [
                'default_value' => new DateTime()
            ]),
            new StringField('REPORT_ID'),
            new StringField('STATUS'),
            new StringField('USER_CREATE'),
            new StringField('MESSAGE')
        ];
    }
}

