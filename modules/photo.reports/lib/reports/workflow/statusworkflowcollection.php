<?php


namespace Photo\Reports\Reports\WorkFlow;


use Toolbox\Core\Collection\Collection;
use Photo\Reports\ModuleConfig;

class StatusWorkflowCollection extends Collection
{
    public function getReworksCount()
    {
        $count = 0;
        foreach ($this as $k => $v) {
            if ($v['STATUS'] == ModuleConfig::STATUS_REWORK){
                $count += 1;
            }
        }

        return $count;
    }

    public function canRework()
    {
        return $this->getReworksCount() < ModuleConfig::MAX_REWORK_COUNT;
    }

}