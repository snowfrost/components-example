<?php


namespace Photo\Reports\Reports\WorkFlow;


use Toolbox\Core\Repository\TableRepository;
use Photo\Reports\ModuleConfig;

class StatusWorkflowRepository extends TableRepository
{
    /**
     * @return StatusWorkflowEntityTable
     */
    public function getEntity()
    {
        return new StatusWorkflowEntityTable();
    }

    public function reportHistory($reportId)
    {
        $data =  $this->getEntity()->getList(['filter' => [
            'REPORT_ID' => $reportId
        ]])->fetchAll();

        return new StatusWorkflowCollection($data);
    }

    public function reportReworksCount($reportId)
    {
        return count( $this->getEntity()->getList(['filter' => [
            'REPORT_ID' => $reportId,
            'STATUS' => ModuleConfig::STATUS_REWORK
        ]])->fetchAll());
    }
}