<?php


namespace Photo\Reports\Reports\WorkFlow;


class StatusWorkflow
{
    private $id;
    private $createAt;
    private $reportId;
    private $status;
    private $userCreate;
    private $message;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return StatusWorkflow
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * @param mixed $createAt
     * @return StatusWorkflow
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReportId()
    {
        return $this->reportId;
    }

    /**
     * @param mixed $reportId
     * @return StatusWorkflow
     */
    public function setReportId($reportId)
    {
        $this->reportId = $reportId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return StatusWorkflow
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserCreate()
    {
        return $this->userCreate;
    }

    /**
     * @param mixed $userCreate
     * @return StatusWorkflow
     */
    public function setUserCreate($userCreate)
    {
        $this->userCreate = $userCreate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     * @return StatusWorkflow
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }
}