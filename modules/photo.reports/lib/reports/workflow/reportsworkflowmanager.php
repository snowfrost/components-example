<?php


namespace Photo\Reports\Reports\WorkFlow;


use Bitrix\Main\Localization\Loc;
use Toolbox\Core\Logger\LoggerTrait;
use Toolbox\Core\Notifications\NoticeInterface;
use Toolbox\Core\Notifications\NotificationService;
use Toolbox\Core\Notifications\ReportNotice;
use Toolbox\Core\Permission\PermissionTrait;
use Toolbox\Core\Structure\StructureService;
use Toolbox\Core\User\User;
use Toolbox\Core\User\UserTrait;
use Photo\Reports\Events\EventTrait;
use Photo\Reports\ModuleConfig;
use Photo\Reports\PhotoReportsService;
use Photo\Reports\Reports\Report;
use Photo\Reports\Reports\StatusEnum;

Loc::loadMessages(__FILE__);

class ReportsWorkFlowManager
{
    use LoggerTrait;
    use PermissionTrait;
    use UserTrait;
    use EventTrait;
    /**
     * @var PhotoReportsService
     */
    private $photoReportsService;
    /**
     * @var StatusWorkflowRepository
     */
    private $statusWorkflowRepository;
    /**
     * @var NotificationService
     */
    private $notificationService;
    /**
     * @var StructureService
     */
    private $structureService;


    /**
     * ReportsWorkFlowManager constructor.
     * @param PhotoReportsService $photoReportsService
     * @param StatusWorkflowRepository $statusWorkflowRepository
     * @param StructureService $structureService
     * @param NotificationService $notificationService
     */
    public function __construct(PhotoReportsService $photoReportsService,
        StatusWorkflowRepository $statusWorkflowRepository,
        StructureService $structureService,
        NotificationService $notificationService)
    {
        $this->photoReportsService = $photoReportsService;
        $this->statusWorkflowRepository = $statusWorkflowRepository;
        $this->notificationService = $notificationService;
        $this->structureService = $structureService;
    }

    public function canChangeStatus(Report $report, $statusCode)
    {
        $status = new StatusEnum();


        switch ($statusCode) {
            case StatusEnum::STATUS_NEW:
                return true;
            case StatusEnum::STATUS_CHECK:

                $photos = $report->getPhotos();
                $task = $report->getTask();

                if ($photos->getActivePhotoCount() < $task->getMinPhotoCount()) {
                    throw new \Exception(Loc::getMessage('PR_WORKFLOW_ERROR_MIN_PHOTO_COUNT', [
                        '#STATUS_CODE#' => $status->getByXmlId($statusCode)['VALUE']
                    ]));
                }

                if ($photos->allActivePhotosNoComments()) {
                    return true;
                } else {
                    throw new \Exception(Loc::getMessage('PR_WORKFLOW_ERROR_CHECK_STATUS_NO_APPROVED_PHOTOS', [
                        '#STATUS_CODE#' => $status->getByXmlId($statusCode)['VALUE']
                    ]));
                }

                break;
            case StatusEnum::STATUS_REWORK:

                $activePhoto = $report->getPhotos()
                    ->getActivePhoto();

                if (!$report->getUfControlDate()) {
                    throw new \Exception(Loc::getMessage('PR_WORKFLOW_ERROR_NO_CONTROL_DATE', [
                        '#STATUS_CODE#' => $status->getByXmlId($statusCode)['VALUE']
                    ]));
                }

                $photos = $report->getPhotos();

                $reworksCount = $this->statusWorkflowRepository->reportReworksCount($report->getId());

                if ($reworksCount >= ModuleConfig::MAX_REWORK_COUNT) {
                    throw new \Exception('Отправлять на переделку можно не более '
                        . ModuleConfig::MAX_REWORK_COUNT
                        . ' раз');
                }

                if (!$photos->allActivePhotosNoComments()) {

                    $photos->approvePhoto();


                    return true;
                } else {
                    throw new \Exception(Loc::getMessage('PR_WORKFLOW_ERROR_ALL_PHOTO_APPROVED', [
                        '#STATUS_CODE#' => $status->getByXmlId($statusCode)['VALUE']
                    ]));
                }

                break;
            case StatusEnum::STATUS_DONE_WARNING:
            case StatusEnum::STATUS_DONE:

                $photos = $report->getPhotos();

                if ($photos->allActivePhotosNoComments()) {
                    return true;
                } else {
                    throw new \Exception(Loc::getMessage('PR_WORKFLOW_ERROR_HAS_NOT_APPROVED_PHOTO',
                        [
                            '#STATUS_CODE#' => $status->getByXmlId($statusCode)['VALUE']
                        ]
                        ));
                }

            case StatusEnum::STATUS_DONE_NOT_VIEW:

                if (!$report->isDone()) {
                    return true;
                }
            default:
                return false;
        }
    }

    public function changeStatus(Report $report, $status)
    {
        global $USER;

        self::log('Start change status to ' . $status);

        if ($this->canChangeStatus($report, $status)) {

            $statusEnum = new StatusEnum();
            $checkStatus = $statusEnum->getByXmlId($status);
            $report->setUfStatus($checkStatus['ID']);
            $this->photoReportsService->registerReport($report);

            $this->statusWorkflowRepository->add([
                'REPORT_ID' => $report->getId(),
                'STATUS' => $status,
                'USER_CREATE' => $USER->GetId(),
                'MESSAGE' => ''
            ]);

            self::changeReportStatus($report->getId(), $status);
            self::log('Status change to ' . $status);
            $this->notify($report, $status);
            return true;
        }
        return false;
    }

    public function notify(Report $report, $status)
    {

        $notice = (new ReportNotice())
            ->setMailNotify(true)
            ->setPushNotify(true);

        switch ($status) {
            case StatusEnum::STATUS_NEW:
                $notice->setTitle(htmlspecialcharsEx('Фотоотчёты'))
                    ->setMessage('Вам поставлен новый фотоотчет #' . $report->getId());
                break;
            case StatusEnum::STATUS_CHECK:
                break;
            case StatusEnum::STATUS_REWORK:
                $notice->setTitle(htmlspecialcharsEx('Фотоотчёты'))
                    ->setMessage('Ваш фотоотчет #' . $report->getId() . ' возвращен на доработку.');
                break;
            case StatusEnum::STATUS_DONE_WARNING:
            case StatusEnum::STATUS_DONE:
            case StatusEnum::STATUS_DONE_NOT_VIEW:
                $notice->setTitle(htmlspecialcharsEx('Фотоотчёты'))
                    ->setMessage('Ваш фотоотчет #' . $report->getId() . ' принят.');
                break;
            default:
                break;
        }

        if (!empty($message = $notice->getMessage())) {
            $notice->setFields(array(
                'ID' => $report->getId(),
                'STATUS' => $message,
            ));

            $this->notifyDepartment($report->getUfMarket(), $notice);
        }
    }

    public function notifyDepartment($departmentId, NoticeInterface &$notice)
    {
        $department = $this->structureService->getDepartmentById($departmentId);
        /** @var User $employee */
        foreach ($department->getEmployees() as $k => $employee) {
            $notice->setUserId($employee->getId());

            $notice->setFields(array_merge(
                $notice->getFields(),
                array(
                    'EMAIL' => $employee->getEmail(),
                    'RECIPIENT' => $employee->getEmail(),
                )
            ));

            $this->notificationService->notify($notice);
        }
    }

    public function checkRights($action = '')
    {
        return true;
    }
}
