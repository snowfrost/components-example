<?php


namespace Photo\Reports\Reports;


use Toolbox\Core\Iblock\IBlockTrait;
use Toolbox\Core\Repository\BaseRepository;
use Toolbox\Core\Repository\IblockElementInterface;
use Toolbox\Core\Repository\IblockElementRepository;
use Toolbox\Core\Repository\Result;
use Photo\Reports\Events\EventTrait;

class PhotoRepository extends IblockElementRepository
{
    use IBlockTrait;
    use EventTrait;
    const MAX_PHOTO_LENGTH = 3;

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return new PhotoEntityTable();
    }


    public function getPhotosByReportId($reportId)
    {
        $data = $this->listElements([ 'SECTION_ID' => $reportId]);
        return new Result($data);
    }


    public function getByParentId($parentId)
    {
        $iblock = $this->findIBlock(PhotoEntityTable::IBLOCK_TYPE, PhotoEntityTable::IBLOCK_CODE);
        $el = new \CIBlockElement();
        $dbr = $el->GetList(['ID' => 'ASC'],
            [
                'IBLOCK_ID' => $iblock['ID'],
                'PROPERTY_PARENT_ID' => $parentId
            ],
            0,
            0,
            [
                '*',
                'PROPERTY_PARENT_ID'
            ]);

        $data = [];

        while ($r = $dbr->GetNext(1, 0)) {
            $data[] = $r;
        }

        $data = $this->mapResult($data);

        return (new Result($data))->current();

    }

    /**
     * @param IblockElementInterface $element
     * @return Result
     * @throws \Exception
     */
    public function flush(IblockElementInterface $element)
    {
        $result = parent::flush($element);

        if ($result->isSuccess()) {
            $this->addReportEvent($result->getId());
        }

        return $result;
    }

    public function getPhotosChain($parentId)
    {
        $data = [];
        $chainLength = 0;

        while ($parentId > 0 && $chainLength < self::MAX_PHOTO_LENGTH) {
            $t = $this->getById($parentId);
            $data[] = $t;
            $parentId = $t->getParentId();
            $chainLength++;
        }

        return  new Result($data);
    }


}