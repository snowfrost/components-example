<?php


namespace Photo\Reports\Reports;


use Bitrix\Main\Type\DateTime;
use CFile;

use Toolbox\Core\Repository\ElementModel;
use Toolbox\Core\Util\ArrayTrait;
use Toolbox\Core\Util\Lang;
use Photo\Reports\Comments\CommentsCollection;

class Photo extends ElementModel
{
    use Lang;

    use ArrayTrait {
        ArrayTrait::toArray as protected toArr;
    }


    const ORIENTATION_LANDSCAPE = 'landscape';
    const ORIENTATION_PORTRAIT = 'portrait';

    private $parentId;
    private $isApprove;
    private $orientation;

    private $fileName;
    private $path;
    private $comments;
    private $photo;

    /**
     * Photo constructor.
     * @param $detailPicture
     * @param $iblockSectionId
     * @param $name
     * @param null $id
     * @internal param $id
     */
    public function __construct($detailPicture, $iblockSectionId, $name, $id = null)
    {
        global $USER;
        $this->iblockSectionId = $iblockSectionId;
        $this->detailPicture = $detailPicture;
        $this->name = $name ?: $this->fileName;
        $this->createdBy = $USER->GetId();
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param mixed $parentId
     * @return Photo
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getPath()
    {
        $this->setPhoto($this->detailPicture);
        return $this->path;
    }

    /**
     * @param mixed $path
     * @return Photo
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDetailPicture()
    {
        return $this->detailPicture;
    }

    /**
     * @param mixed $detailPicture
     * @return Photo
     */
    public function setDetailPicture($detailPicture)
    {
        $this->setPhoto($detailPicture, true);
        $data = CFile::MakeFileArray(\CFile::GetPath($detailPicture));
        $this->detailPicture = $data;

        return $this;
    }


    /**
     * @return CommentsCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param $comments
     * @return Photo
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        $this->setPhoto($this->detailPicture);
        return $this->photo;
    }

    /**
     * @param mixed $photoId
     * @param bool $update
     * @return Photo
     */
    public function setPhoto($photoId, $update = false)
    {
        if (is_null($this->photo) || $update) {
            $this->photo = \CFile::GetFileArray($photoId);
            $this->path = $this->photo['SRC'];
            $this->fileName = $this->photo['ORIGINAL_NAME'];
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        $this->setPhoto($this->detailPicture);
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     * @return Photo
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
        return $this;
    }

    public function setLandscapeOrientation()
    {
        $this->orientation = self::ORIENTATION_LANDSCAPE;
    }

    public function setPortraitOrientation()
    {
        $this->orientation = self::ORIENTATION_PORTRAIT;
    }

    public function getDetailFilePath()
    {
        $this->getPhoto();
        return $this->photo['SRC'];
    }

    /**
     * @return mixed
     */
    public function getIsApprove()
    {
        return $this->isApprove;
    }

    /**
     * @param mixed $isApprove
     * @return Photo
     */
    public function setIsApprove($isApprove)
    {
        $this->isApprove = $isApprove;
        return $this;
    }

    public function hasComments()
    {
        return $this->comments->count() > 0;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $this->setDetailPicture($this->detailPicture);
        return $this->toArr();
    }

    public function init()
    {
        $this->getPhoto();
    }

}