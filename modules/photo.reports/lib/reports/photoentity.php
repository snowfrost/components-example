<?php


namespace Photo\Reports\Reports;


use Toolbox\Core\Repository\ElementEntity;
use Photo\Reports\ModuleConfig;

class PhotoEntityTable extends ElementEntity
{
    const IBLOCK_TYPE = ModuleConfig::IBLOCK_TYPE;
    const IBLOCK_CODE = ModuleConfig::IBLOCK_CODE;
}