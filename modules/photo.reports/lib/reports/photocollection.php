<?php


namespace Photo\Reports\Reports;


use Toolbox\Core\Collection\Collection;
use Toolbox\Core\User\User;
use Toolbox\Core\User\UserTrait;
use Photo\Reports\Comments\Comment;
use Photo\Reports\Permission\RoleResolver;
use Photo\Reports\PhotoReportsService;

class PhotoCollection extends Collection
{
    use UserTrait;

    /**
     * PhotoCollection constructor.
     * @param array $elements
     */
    public function __construct(array $elements = [])
    {
        uasort($elements, function ($a, $b){
            /** @var Photo $a */
            /** @var Photo $b */
            if ($a->getSort() == $b->getSort()) {
                return 0;
            }
            return ($a->getSort() < $b->getSort()) ? -1 : 1;
        });

        parent::__construct($elements);
    }


    public function allActivePhotosNoComments()
    {
        /** @var PhotoReportsService $photoReportService */
        $photoReportService = \CoreContainer::find('photoreport_service');

        /** @var Photo $photo */
        foreach ($this->getActivePhoto() as $key => $photo) {
            if ($photo->isActive()) {
                if ($photo->hasComments()){
                    /** @var Comment $lastComment */
                    $lastComment = $photo->getComments()->last() ;

                    $userId = $lastComment->getUserCreate();
                    /** @var User $user */
                    $user = $photoReportService->getModuleUser($userId);
                    if (RoleResolver::userIsMerchandiser($user)
                        || RoleResolver::userIsAdmin($user)) {

                        return false;
                    }
                }
            }
        }

        return true;
    }

    public function approvePhoto()
    {
        $approveEnum = new ApproveEnum();

        $value = $approveEnum->getByXmlId('Y');

        /** @var Photo $photo */
        foreach ($this->getActivePhoto() as $k => $photo) {
            if (!$photo->hasComments()) {
                $photo->setIsApprove($value['ID']);
            }
        }
    }

    public function getActivePhoto()
    {
        return $this->filter(function($v){
            /** @var Photo $v */
            return $v->isActive();
        });
    }

    public function getActivePhotoCount()
    {
        return count($this->getActivePhoto());
    }

    public function getNewPhotoNumber()
    {
        $number = 0;
        foreach ($this->getActivePhoto() as $key => $photo) {
            if ($number < $photo->getSort()) {
                $number = $photo->getSort();
            }
        }

        return ++$number;
    }

    public function normalizeNumbers()
    {
        /** @var  Photo $photo */
        foreach ($this->getActivePhoto() as $k => $photo) {
            $photo->setSort(++$k);
        }
    }

    public function getById($photoId)
    {
        return current( $this->filter(function($v) use ($photoId) {
            /** @var Photo $v */
            return ($v->getId() == $photoId);
        }));
    }

    public function getPhotoChain(Photo $photo)
    {
        $find = true;

        $photoId = $photo->getParentId();
        $result[] = $photo->toArraySensitive(1);

        while ($find) {
            /** @var Photo $p */
            foreach ($this as $k => $p) {

                if ($p->getId() == $photoId) {
                    $result[] = $p->toArraySensitive(1);
                    $photoId = $p->getParentId();
                    continue;
                }

                $find = false;
            }
        }

        return $result;
    }

    public function toArraySensitive($upper)
    {
        $result = [];
        /** @var Photo $photo */
        foreach ($this as $k => $photo) {
            $t = $photo->toArraySensitive(1);
            $t['COMMENTS'] = $photo->getComments()->toArray();
            $result[] = $photo;
        }

        return $result;
    }
}