<?php


namespace Photo\Reports\Reports;


use Toolbox\Core\Collection\Collection;

class ReportCollection extends Collection
{
    public function getStatData()
    {
        $data = [
            'total' => $this->count(),
            'process' => 0,
            'finish' => 0
        ];

        /**
         * @var  $k
         * @var Report $report
         */

        $statusEnum = new StatusEnum();

        foreach ($this as $k => $report) {
            $status =  $statusEnum->getById($report->getUfStatus());

            $report->setStatus($status);

            if ($report->isDone()) {
                $data['finish']++;
            } else {
                $data['process']++;
            }
        }



        return $data;
    }
}