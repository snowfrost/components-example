<?php


namespace Photo\Reports\Reports;


use Toolbox\Core\Util\ArrayTrait;
use Toolbox\Core\Util\Lang;
use Toolbox\Core\Structure\Markets\Market;
use Photo\Reports\ModuleConfig;
use Photo\Reports\Reports\WorkFlow\StatusWorkflowCollection;
use Photo\Reports\Tasks\Task;

class Report
{
    use ArrayTrait {
        ArrayTrait::toArray as protected toArr;
    }
    use Lang;

    const STATUS_NEW = ModuleConfig::STATUS_NEW;
    const STATUS_CHECK = ModuleConfig::STATUS_CHECK;
    const STATUS_REWORK = ModuleConfig::STATUS_REWORK;
    const STATUS_DONE = ModuleConfig::STATUS_DONE;
    const STATUS_DONE_WARNING = ModuleConfig::STATUS_DONE_WARNING;
    const STATUS_DONE_NOT_VIEW = ModuleConfig::STATUS_DONE_NOT_VIEW;

    private $id;
    private $timestampX;
    /** @var \Bitrix\Main\Type\DateTime */
    private $dateCreate;
    private $name;
    private $code;
    private $xmlId;
    private $createdByUser;
    private $modifiedByUser;
    private $iblockSectionId;
    private $sectionPageUrl;

    private $ufMarket;
    private $ufRating;
    private $ufIsApprove;
    /** @var \Bitrix\Main\Type\DateTime
     * @default null
     */
    private $ufControlDate;
    private $ufStatus;
    private $ufReport = 'Y';
    private $ufTaskId;
    /** @var \Bitrix\Main\Type\DateTime */
    private $ufDeadline;

    private $photos;
    private $comments = [];
    private $history = [];
    private $market;
    private $status;
    private $task;
    private $photosCollection;
    private $workflowHistory;

    /**
     * Report constructor.
     * @param $id
     * @param $ufTaskId
     * @param null $ufMarket
     * @param null $name
     * @internal param null $taskId
     */
    public function __construct($ufTaskId, $ufMarket, $name, $id)
    {
        $this->id = $id;
        $this->ufTaskId = $ufTaskId;
        $this->ufMarket = $ufMarket;
        $this->name = $name;
    }

    public function addPhoto(Photo $photo)
    {
        $this->photos[] = $photo;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     * @return Report
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimestampX()
    {
        return $this->timestampX;
    }

    /**
     * @param mixed $timestampX
     * @return Report
     */
    public function setTimestampX($timestampX)
    {
        $this->timestampX = $timestampX;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * @param mixed $dateCreate
     * @return Report
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Report
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     * @return Report
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedByUser()
    {
        return $this->createdByUser;
    }

    /**
     * @param mixed $createdByUser
     * @return Report
     */
    public function setCreatedByUser($createdByUser)
    {
        $this->createdByUser = $createdByUser;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModifiedByUser()
    {
        return $this->modifiedByUser;
    }

    /**
     * @param mixed $modifiedByUser
     * @return Report
     */
    public function setModifiedByUser($modifiedByUser)
    {
        $this->modifiedByUser = $modifiedByUser;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUfMarket()
    {
        return $this->ufMarket;
    }

    /**
     * @param mixed $ufMarket
     * @return Report
     */
    public function setUfMarket($ufMarket)
    {
        $this->ufMarket = $ufMarket;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUfStatus()
    {
        return $this->ufStatus;
    }

    /**
     * @param mixed $ufStatus
     * @return Report
     */
    public function setUfStatus($ufStatus)
    {
        $this->ufStatus = $ufStatus;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUfDeadline()
    {
        return $this->ufDeadline;
    }

    /**
     * @param mixed $ufDeadline
     * @return Report
     */
    public function setUfDeadline($ufDeadline)
    {
        $this->ufDeadline = $ufDeadline;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUfControlDate()
    {
        return $this->ufControlDate;
    }

    /**
     * @param mixed $ufControlDate
     * @return Report
     */
    public function setUfControlDate($ufControlDate)
    {
        $this->ufControlDate = $ufControlDate;
        return $this;
    }

    /**
     * @return PhotoCollection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @param mixed $photos
     * @return Report
     */
    public function setPhotos(PhotoCollection $photos)
    {
        $this->photos = $photos;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param mixed $comments
     * @return Report
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * @param mixed $history
     * @return Report
     */
    public function setHistory($history)
    {
        $this->history = $history;
        return $this;
    }

    /**
     * @return Market
     */
    public function getMarket()
    {
        return $this->market;
    }

    /**
     * @param Market $market
     * @return Report
     */
    public function setMarket(Market $market)
    {
        $this->market = $market;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getXmlId()
    {
        return $this->xmlId;
    }

    /**
     * @param mixed $xmlId
     * @return Report
     */
    public function setXmlId($xmlId)
    {
        $this->xmlId = $xmlId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUfTaskId()
    {
        return $this->ufTaskId;
    }

    /**
     * @param mixed $ufTaskId
     * @return Report
     */
    public function setUfTaskId($ufTaskId)
    {
        $this->ufTaskId = $ufTaskId;
        return $this;
    }

    public function countPhotos()
    {
        return count($this->photos);
    }

    /**
     * @return mixed
     */
    public function getUfRating()
    {
        return $this->ufRating;
    }

    /**
     * @param mixed $ufRating
     * @return Report
     */
    public function setUfRating($ufRating)
    {
        $this->ufRating = $ufRating;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUfIsApprove()
    {
        return $this->ufIsApprove;
    }

    /**
     * @param mixed $ufIsApprove
     * @return Report
     */
    public function setUfIsApprove($ufIsApprove)
    {
        $this->ufIsApprove = $ufIsApprove;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Report
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }


    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param Task $task
     * @return Report
     */
    public function setTask(Task $task)
    {
        $this->task = $task;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUfReport()
    {
        return $this->ufReport;
    }

    /**
     * @param mixed $ufReport
     * @return Report
     */
    public function setUfReport($ufReport)
    {
        $this->ufReport = $ufReport;
        return $this;
    }

    public function statusDone()
    {
        $this->status = self::STATUS_DONE;
    }

    /**
     * @return mixed
     */
    public function getIblockSectionId()
    {
        return $this->iblockSectionId;
    }

    /**
     * @param mixed $iblockSectionId
     * @return Report
     */
    public function setIblockSectionId($iblockSectionId)
    {
        $this->iblockSectionId = $iblockSectionId;
        return $this;
    }

    /** @return Report */
    public static function createReport()
    {
        $r = new \ReflectionClass(Report::class);
        return $r->newInstanceWithoutConstructor();
    }

    public function toArray($upper = false)
    {
        $result = [];
        $data = $this->toArr();
        foreach ($data as $k => $v) {
            if (is_object($v) && method_exists($v, 'toArray')) {
                $v = $v->toArray(1);
            }

            if ($upper) {
                $result[strtoupper(self::toUnderscore($k))] = $v;
            } else {
                $result[self::toUnderscore($k)] = $v;
            }
        }

        return $result;

    }

    /**
     * @return mixed
     */
    public function getSectionPageUrl()
    {
        return $this->sectionPageUrl;
    }

    /**
     * @param mixed $sectionPageUrl
     * @return Report
     */
    public function setSectionPageUrl($sectionPageUrl)
    {
        $this->sectionPageUrl = $sectionPageUrl;
        return $this;
    }

    public function getPhotosPrevNext($photoId)
    {
        $prev = null;
        $next = null;

        $filtered = $this->photos->getActivePhoto();

        $t = array_map(function ($v) {
            return $v->getId();
        }, $filtered);

        $clearData =  array_values($t);

        $key = array_search($photoId, $clearData );


        $prev = ($key > 0) ? $key - 1 : null;
        $next = ($key < (count($clearData) - 1) && ((count($clearData) - 1) > 0)) ? $key + 1 : null;


        $res =  [
            'PREV' => !is_null($prev) ? $this->photos->getById($clearData[$prev])->getId() : null,
            'NEXT' => !is_null($next) ? $this->photos->getById($clearData[$next])->getId() : null,
        ];

        return $res;
    }

    /**
     * @return StatusWorkflowCollection
     */
    public function getWorkflowHistory()
    {
        return $this->workflowHistory;
    }

    /**
     * @param mixed $workflowHistory
     * @return Report
     */
    public function setWorkflowHistory(StatusWorkflowCollection $workflowHistory)
    {
        $this->workflowHistory = $workflowHistory;
        return $this;
    }

    public function isRework()
    {
        return $this->status['XML_ID'] == ModuleConfig::STATUS_REWORK;
    }

    public function isCheck()
    {
        return $this->status['XML_ID'] == ModuleConfig::STATUS_CHECK;
    }

    public function isNew()
    {
        return $this->status['XML_ID'] == ModuleConfig::STATUS_NEW;
    }

    public function isDone()
    {
        return in_array($this->status['XML_ID'],
            [
                ModuleConfig::STATUS_DONE_WARNING,
                ModuleConfig::STATUS_DONE,
                ModuleConfig::STATUS_DONE_NOT_VIEW]);
    }




}