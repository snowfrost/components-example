<?php


namespace Photo\Reports\Reports;


use Toolbox\Core\Repository\Enum\ListEnum;
use Photo\Reports\ModuleConfig;

class ApproveEnum extends ListEnum
{
    const IBLOCK_CODE = ModuleConfig::IBLOCK_CODE;
    const IBLOCK_TYPE = ModuleConfig::IBLOCK_TYPE;
    const CODE = ModuleConfig::PROPERTY_PHOTO_IS_APPROVE;
}