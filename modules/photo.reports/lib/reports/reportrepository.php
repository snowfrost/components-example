<?php


namespace Photo\Reports\Reports;


use Bitrix\Main\Error;
use Toolbox\Core\Iblock\IBlockTrait;
use Toolbox\Core\Repository\IblockSectionRepository;
use Toolbox\Core\Repository\Result;
use CPageOption;
use Photo\Reports\Events\EventTrait;

class ReportRepository extends IblockSectionRepository
{
    use IBlockTrait;
    use EventTrait;

    const ELEMENTS_PER_PAGE = 10;

    /**
     * @return ReportEntityTable
     */
    public function getEntity()
    {
        return new ReportEntityTable();
    }

    public function listSections(array $filter = [])
    {
        $iblock = $this->getEntity()->getIBlock();
        $filter['IBLOCK_ID'] = $iblock['ID'];
        $filter['UF_REPORT'] = 0;

        $dbr = \CIBlockSection::GetList(["left_margin"=>"asc"],
            $filter,
            false,
            [
                'UF_*'
            ]);
        $data = [];

        $arParents = [];

        while ($r = $dbr->GetNext(1, 0)) {
            if ($r['IBLOCK_SECTION_ID']) {
                $arParents[] = $r['IBLOCK_SECTION_ID'];
            }
            $data[] = $r;
        }


        foreach ($data as $k => &$v){
            if (in_array($v['ID'],$arParents)) {
                $v['IS_PARENT'] = true;
            }
        }

        return new Result($data);
    }

    public function listReports(array $filter = [], $pagenation = false, $sort = [])
    {
        if (empty($sort)) {
            $sort = ['ID' => "ASC"];
        }
        CPageOption::SetOptionString("main", "nav_page_in_session", "N");

        $iblock = $this->getEntity()->getIBlock();
        $filter['IBLOCK_ID'] = $iblock['ID'];
        $filter['UF_REPORT'] = 1;


        $pagen = ($pagenation) ? [
            'nPageSize' => self::ELEMENTS_PER_PAGE
        ] : false;

        $dbr = \CIBlockSection::GetList($sort,
            $filter,
            false,
            [
               'UF_*'
            ], $pagen);
        $data = [];


        while ($r = $dbr->GetNext(1, 0)) {
            $data[] = $r;
        }

        $data = $this->mapResult($data);


        return (new Result($data))->setNavString(
            $dbr->GetPageNavString('', 'infinity-waypoints'));
    }

    public function getById($reportId)
    {
        $result = $this->listReports(['ID' => $reportId]);
        if ($result->count() > 0) {
            return $result;
        } else {
            $result->addError(new Error('Empty result'));
            return $result;
        }
    }

    public function getByTaskId($taskId, $filter = [], $pagenation = false,  $sort = [])
    {
        $filter['UF_TASK_ID'] = $taskId;
        $r =  $this->listReports($filter, $pagenation, $sort);
        return $r;
    }

    public function flush(Report $report)
    {
        $section = new \CIBlockSection();
        $arFields = $report->toArray();
        $data = $this->prepareFields($arFields);
        $data['IBLOCK_ID'] = $this->getEntity()->getIBlockId();
        $result = new Result();

        if ($report->getId()) {
            $section->Update($report->getId(), $data);
            $result->setId($report->getId());
        } else {
            if($id =  $section->Add($data)){
                $this->addReportEvent($id);
                $report->setId($id);
                $result->setId($id);
            } else {
                $result->addError(new Error($section->LAST_ERROR));
            }
        }

        return $result;
    }


    public function createReportSection(array $taskInfo)
    {
        self::log('Check section by task ' . $taskInfo['ID']);

        $rootCode = $taskInfo['TYPE'];
        $reportSectionName = $taskInfo['SECTION_NAME'];
        $reportSectionCode = $this->translit($reportSectionName);

        self::log('Root directory = ' . $rootCode . ' enum xml-id ', $rootCode);

        if ($rootParent = $this->getSectionByCode($rootCode)) {

            self::log('Parent root = ' . $rootParent['NAME'], [$rootParent['ID']]);

            $fields = [
                'IBLOCK_ID' => $this->getEntity()->getIBlockId(),
                'CODE' => $reportSectionCode . $rootParent['ID'],
                'NAME' => $reportSectionName,
                'IBLOCK_SECTION_ID' => $rootParent['ID'],
                'UF_IS_TASK' => 'Y',
                'UF_TASK_ID' => $taskInfo['TASK_ID']
            ];

            self::log('Sub section ' . $reportSectionName, [$fields]);


            $sectionSecond = $this->createSection($fields);

            if ($sectionSecond->isSuccess()) {
                self::log('Sub section id = ' . $sectionSecond->getId());
               return $sectionSecond->getId();
            }
        }


        return false;
    }

    public function countFinisherReports($sectionId)
    {
        $iblock = $this->getEntity()->getIBlock();
        $filter['IBLOCK_ID'] = $iblock['ID'];
        $filter['SECTION_ID'] = $sectionId;
        $filter['UF_REPORT'] = 1;


        $dbr = \CIBlockSection::GetList(['ID' => 'ASC'],
            $filter,
            false,
            [
                'UF_*'
            ]);
        $data = [
            'FINISHED' => 0,
            'IN_PROGRESS' => 0,
            'TOTAL' => 0
        ];

        $status = new StatusEnum();

        $arStatus = array_column( [
            $status->getByXmlId(StatusEnum::STATUS_DONE),
            $status->getByXmlId(StatusEnum::STATUS_DONE_NOT_VIEW),
            $status->getByXmlId(StatusEnum::STATUS_DONE_WARNING),
            $status->getByXmlId(StatusEnum::STATUS_CHECK),
            $status->getByXmlId(StatusEnum::STATUS_REWORK),
        ], 'ID');

        $arStatusFinish = array_column( [
            $status->getByXmlId(StatusEnum::STATUS_DONE),
            $status->getByXmlId(StatusEnum::STATUS_DONE_NOT_VIEW),
            $status->getByXmlId(StatusEnum::STATUS_DONE_WARNING),
        ], 'ID');




        while ($r = $dbr->GetNext(1, 0)) {
            $data['TOTAL'] +=1;

            if (in_array($r['UF_STATUS'], $arStatus)) {
                $data['IN_PROGRESS'] +=1;
            }

            if (in_array($r['UF_STATUS'], $arStatusFinish)) {
                $data['FINISHED'] +=1;
            }
        }


        return new Result($data);
    }
}
