<?php


namespace Photo\Reports\Reports;


use Bitrix\Iblock\SectionTable;
use Toolbox\Core\Iblock\IBlockTrait;
use Photo\Reports\ModuleConfig;

class ReportEntityTable extends SectionTable
{
    use IBlockTrait;

    const IBLOCK_TYPE = ModuleConfig::IBLOCK_TYPE;
    const IBLOCK_CODE = ModuleConfig::IBLOCK_CODE;
}