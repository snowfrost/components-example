<?php


namespace Photo\Reports\Reports;


use Toolbox\Core\Repository\Enum\UserFieldEnum;
use Photo\Reports\ModuleConfig;

class StatusEnum extends UserFieldEnum
{
    const CODE = 'UF_STATUS';


    const STATUS_NEW = ModuleConfig::STATUS_NEW;
    const STATUS_CHECK = ModuleConfig::STATUS_CHECK;
    const STATUS_REWORK = ModuleConfig::STATUS_REWORK;
    const STATUS_DONE = ModuleConfig::STATUS_DONE;
    const STATUS_DONE_WARNING = ModuleConfig::STATUS_DONE_WARNING;
    const STATUS_DONE_NOT_VIEW = ModuleConfig::STATUS_DONE_NOT_VIEW;

    const MARKET_STATUS_DONE = ModuleConfig::MARKET_STATUS_DONE;

}