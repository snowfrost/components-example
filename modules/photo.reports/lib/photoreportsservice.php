<?php


namespace Photo\Reports;


use Bitrix\Main\Type\DateTime;
use Toolbox\Core\Iblock\IBlockTrait;
use Toolbox\Core\Logger\LoggerTrait;
use Toolbox\Core\Repository\Result;
use Toolbox\Core\Util\Lang;
use CoreContainer;
use Toolbox\Core\Structure\Markets\Market;
use Toolbox\Core\Structure\Markets\MarketRepository;
use Toolbox\Core\User\UserService;
use Photo\Reports\Comments\Comment;
use Photo\Reports\Comments\CommentRepository;

use Photo\Reports\Comments\CommentsCollection;
use Photo\Reports\Events\EventRepository;
use Photo\Reports\Events\EventTrait;
use Photo\Reports\Permission\PermissionService;
use Photo\Reports\Permission\PermissionTrait;
use Photo\Reports\Reports\Photo;
use Photo\Reports\Reports\PhotoCollection;
use Photo\Reports\Reports\PhotoRepository;
use Photo\Reports\Reports\Report;
use Photo\Reports\Reports\ReportCollection;
use Photo\Reports\Reports\ReportRepository;
use Bitrix\Main\Localization\Loc;
use Photo\Reports\Reports\StatusEnum;
use Photo\Reports\Reports\WorkFlow\StatusWorkflowRepository;
use Photo\Reports\Reports\WorkFlow\WorkFlowTrait;
use Photo\Reports\Tasks\Task;
use Photo\Reports\Tasks\TaskRepository;
use Photo\Reports\Tasks\TaskTypeEnum;
use Photo\Reports\Types\ReportTypeEnum;

Loc::loadMessages(__FILE__);

class PhotoReportsService
{
    use IBlockTrait;
    use PermissionTrait;
    use LoggerTrait;
    use EventTrait;
    use Lang;
    use WorkFlowTrait;

    /**
     * @var ReportRepository
     */
    private $reportRepository;
    /**
     * @var PhotoRepository
     */
    private $photoRepository;
    /**
     * @var CommentRepository
     */
    private $commentRepository;
    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var MarketRepository
     */
    private $marketRepository;
    /**
     * @var TaskRepository
     */
    private $taskRepository;
    /**
     * @var StatusWorkflowRepository
     */
    private $statusWorkflowRepository;

    #TODO BAD PRACTICE IMPL MEMORY CACHE SERVICE
    private $reportCache = [];
    #TODO BAD PRACTICE IMPL MEMORY CACHE SERVICE
    private $usersCache = [];

    public function __construct(
        ReportRepository $reportRepository,
        PhotoRepository $photoRepository,
        CommentRepository $commentRepository,
        EventRepository $eventRepository,
        MarketRepository $marketRepository,
        TaskRepository $taskRepository,
        StatusWorkflowRepository $statusWorkflowRepository
    )
    {

        $this->reportRepository = $reportRepository;
        $this->photoRepository = $photoRepository;
        $this->commentRepository = $commentRepository;
        $this->eventRepository = $eventRepository;
        $this->marketRepository = $marketRepository;
        $this->taskRepository = $taskRepository;
        $this->statusWorkflowRepository = $statusWorkflowRepository;
    }

    public function result($id = null, $data = [], array $errors = [])
    {
        $r = new Result($data);

        if (!empty($errors)) {
            $r->addErrors($errors);
        }

        if (!is_null($id)) {
            $r->setId($id);
        }

        return $r;
    }


    public function setStatusReportById($reportId, $status)
    {
        $r = $this->getReportById($reportId);
        if ($r->getId()) {
            $statusEnum = new StatusEnum();
            $newStatus = $statusEnum($status);
            $r->setUfStatus($newStatus['ID']);
            $this->reportRepository->flush($r);
        }

        return $this->result($r->getId(), [$r]);
    }

    public function getReportIBlock()
    {
        return $this->findIBlock(ModuleConfig::IBLOCK_TYPE, ModuleConfig::IBLOCK_CODE);
    }

    public function getTaskById($id)
    {
        return $this->taskRepository->getById($id);
    }

    public function getReportById($id, $fillReport= false)
    {
        $result = $this->reportRepository->getById($id);
        if ($result->isSuccess()) {
            /** @var Report $report */
            $report = $result->current();
            if ($fillReport){
                $this->fillReport($report);
            }
            return $report;
        }

        throw new \Exception(Loc::getMessage('REPORT_NOT_EXIST', ['#ID#' => $id]));
    }

    public function getReportsByTaskId($id)
    {
        return $this->reportRepository->getByTaskId($id);
    }

    public function getReportsBySectionId($sectionId)
    {
        return $this->reportRepository->listReports([
            'SECTION_ID' => $sectionId
        ]);
    }

    public function getTaskSectionById($sectionId)
    {
        $t = $this->reportRepository->listSections([
            'ID' => $sectionId
        ])->getData();

        return array_shift($t);
    }


    public function registerTask(Task $task, $createReports = false)
    {
        $result = $this->taskRepository->flush($task);

        if ($result->isSuccess()) {
            #TODO Implement register rights on task if needed
        }

        /** @var Task $task */
        $task = $this->taskRepository->getById($task->getId());

        $dataResult = [];

        if ($createReports) {

            $markets = $task->getMarkets();
            $statusEnum = new StatusEnum();
            $newStatus = $statusEnum(Report::STATUS_NEW);

            $taskTypeEnum = new TaskTypeEnum();
            $type = $taskTypeEnum->getById($task->getReportType());

            $taskInfo = [
                'TASK_ID' => $task->getId(),
                'TYPE' => $type['XML_ID'],
                'TYPE_ID' => $type['ID'],
                'SECTION_NAME' => $task->getTaskName(),
            ];

            $iblockSectionId = $this->reportRepository->createReportSection($taskInfo);

            foreach ($markets as $marketId) {

                /** @var Market $market */
                $market = $this->marketRepository->getById($marketId)
                    ->current();
                if ($market) {

                    $report = Report::createReport();

                    $report->setUfTaskId($task->getId())
                        ->setIblockSectionId($iblockSectionId)
                        ->setUfMarket($market->getId())
                        ->setUfStatus($newStatus['ID'])
                        ->setUfDeadline($task->getDeadline())
                        ->setName(implode(', ',
                            [
                                self::replaceDoubleQuotesToSingle($task->getName()) ,
                                self::replaceDoubleQuotesToSingle($market->getName())]));

                    $r = $this->registerReport($report);

                    if ($r->isSuccess()) {
                        $dataResult[] = $report->getId();
                        self::setNewStatus($report);
                        $this->registerReportPermissionOnDepartment($r->getId(), $market->getId());
                        #$this->registerReportPermissionOnDepartment($r->getId(), $market->getId());
                        self::info('Report create id=' . $report->getId() . ' for task id=' . $task->getId());
                    } else {
                        self::error('Photo report create', $r->getErrorMessages());
                    }
                } else {
                    self::error('Market not exist id=' . $marketId . ' in task id=' . $task->getId());
                }

            }

        }

        self::info('Task created ' . $task->getId());

        return $this->result($task->getId(), $dataResult);

    }

    public function registerReport(Report $report)
    {

        $result = new Result();

        $reportResult = $this->reportRepository->flush($report);

        if ($report->countPhotos() > 0) {
            if ($report->isNew()) {
                $report->getPhotos()->normalizeNumbers();
            }
            /** @var Photo $photo */
            foreach ($report->getPhotos() as $photo) {
                self::log('Photo ',  [$photo]);
                $this->photoRepository->flush($photo);
            }
        }

        if ($reportResult->isSuccess()) {
            $result->setId($reportResult->getId());
            self::info('Report flush ' . $report->getId());
        } else {
            self::error('Report flush error', [$reportResult->getErrorMessages()]);
        }

        return $reportResult;
    }

    public function registerComment(Comment $comment)
    {
        global $USER;
        $createDate = new DateTime();
        $comment->setUserCreate($USER->GetId())
            ->setUserUpdate($USER->GetId())
            ->setCreateAt($createDate)
            ->setUpdateAt($createDate);

        return $this->commentRepository->flush($comment);
    }

    public function registerPhoto(Photo $photo)
    {
        return $this->photoRepository->flush($photo);
    }

    public function fillReport(Report $report, $readFromCache = true)
    {
        if ($report->getId()) {

            if (array_key_exists($report->getId(), $this->reportCache) && $readFromCache) {
                $this->fillReportFromCache();
            }


            $photoCollection = $this->photoRepository->getPhotosByReportId($report->getId());

            $report->setPhotos(new PhotoCollection($photoCollection->getData()) );
            $ids = array_column($photoCollection->toArray(1), 'ID');

            $users = [];


            if (is_array($ids) && !empty($ids)) {
                /** @var Photo $photo */
                foreach ($photoCollection as $k => $photo) {
                    $photosComments = $this->commentRepository->getPhotoComments($photo->getId());
                    $usersCreate = array_column($photosComments->toArray(1), 'USER_CREATE');

                    if (is_array($usersCreate)) {
                        $users = array_merge($users, $usersCreate);
                    }


                    $photo->setComments(new CommentsCollection($photosComments->getData()) );
                }
            }

            foreach ($users as $user) {
                $this->getModuleUser($user);
            }

            $workFlowHistory = $this->statusWorkflowRepository->reportHistory($report->getId());
            $report->setWorkflowHistory($workFlowHistory);

            $market = $this->marketRepository->getById($report->getUfMarket());
            $report->setMarket($market->current());
            $comments = $this->commentRepository->getReportComments($report->getId())
                ->getData();
            $report->setComments($comments);
            $task = $this->taskRepository->getById($report->getUfTaskId());

            $status = new StatusEnum();

            $report->setStatus($status->getById($report->getUfStatus()));
            $report->setTask($task);

        }

        return $report;
    }

    /** TODO IMPL fill report data from cache */
    public function fillReportFromCache()
    {

    }

    public function createTaskFromArray(array $data)
    {
        return current($this->taskRepository->mapResult($data));
    }

    public function getModuleUser($userId)
    {
        #TODO BAD PRACTICE IMPL MEMORY CACHE SERVICE
        if (array_key_exists($userId, $this->usersCache)) {
            return $this->usersCache[$userId];
        }

        /** @var UserService $userService */
        $userService = CoreContainer::find('fp_user_service');
        /** @var PermissionService $ps */
        $ps = CoreContainer::find('pr_permission_service');
        $user = $userService->buildUser($userId);
        $ps->fillUserPermissions($user);

        #TODO BAD PRACTICE IMPL MEMORY CACHE SERVICE
        $this->usersCache[$userId] = $user;

        return $user;
    }

    public function deletePhotoFromReport($reportId, $photoId){
        $report = $this->getReportById($reportId,1);
        $photos = $report->getPhotos();
        /** @var Photo $photo */
        $photo = $photos->getById($photoId);
        $this->photoRepository->delete($photoId);
        $parentId = $photo->getParentId();
        /** @var Photo $parentPhoto */
        if ($parentPhoto = $photos->getById($parentId)){
            $parentPhoto->setActive('Y');
            $this->registerPhoto($parentPhoto);
        }

        return $parentPhoto;
    }

    public function getStatisticData(array $filter = [])
    {
        $tasks= $this->taskRepository->listElements($filter);
        $data =  [];

        $reportTypeEnum = new TaskTypeEnum();
        /**
         * @var $k
         * @var Task $task
         */
        foreach ($tasks as $k => $task) {
            $reportsResult = $this->getReportsByTaskId($task->getId());
            if ($reportsResult->count() >  0 ){
                $reportCollection = new ReportCollection($reportsResult->getData());
                $type = $reportTypeEnum->getById($task->getReportType());
                if (array_key_exists($type['XML_ID'], $data[$task->getDateCreate()->format('M')])) {
                    $newData = $reportCollection->getStatData();
                    $data[$task->getDateCreate()->format('M')][$type['XML_ID']]['total'] += $newData['total'];
                    $data[$task->getDateCreate()->format('M')][$type['XML_ID']]['process'] += $newData['process'];
                    $data[$task->getDateCreate()->format('M')][$type['XML_ID']]['finish'] += $newData['finish'];

                } else {
                    $data[$task->getDateCreate()->format('M')][$type['XML_ID']] = $reportCollection->getStatData();
                }

            }
        }

        return $data;
    }
}